package recursion;

/**
 * @author dali
 * @date 2021-01-26 20:18
 * @Description
 */

public class BaHuangHou {
    public static void main(String[] args) {
        //创建一个8x8的二维数组
        int[][] qipan = new int[8][8];
        cal(qipan, 0, 0);
        System.out.println("棋盘落子情况为：");
        for (int[] qis : qipan) {
            for (int qi : qis) {
                System.out.print(qi + " ");
            }
            System.out.println();
        }
    }

    //0表示尚未落子，1表示落子，2表示不能落子
    public static int cal(int[][] qipan, int i, int j) {
        if (i > 8) {
            //找到所有点
            return 0;
        }
        if (j > 8) {
            //失败，没有找到那个落子点，需要回溯
            return 1;
        }
        if (qipan[i][j] == 2) {
            //绘制一个过度棋盘
            int[][] qipans = qipan;
            int a = i;
            int b = j;
            //证明该点不可用，到同行的下一列寻找
            if (cal(qipans, a, ++b) == 1) {
                //在去原有列的下一列寻找
                cal(qipan, i, ++j);
            }
        }
        //落子运算
        luozi(qipan, i, j);
        System.out.println();
        //这一行结束，到下一行寻找
        cal(qipan, ++i, 0);
        return 0;
    }

    public static void luozi(int[][] qipan, int i, int j) {
        //所有横竖都为2，2表示不可以在落子
        for (int k = 0; k < 8; k++) {
            if (qipan[i][j] != 1) {
                qipan[i][k] = 2;
                qipan[k][j] = 2;
            }
        }
        //过度变量a,b
        int a = i;
        int b = j;
        //将起始点设为0
        while (a != 0 && b != 0) {
            a--;
            b--;
        }
        //从左到右斜
        do {
            qipan[a][b] = 2;
            a++;
            b++;
        } while (a != 8 && b != 8 && qipan[a][b] != 1);
        //重设起始点
        a = i;
        b = j;
        //从右到左斜
        do {
            qipan[a][b] = 2;
            a++;
            b--;
        } while (a != 8 && b != -1 && qipan[a][b] != 1);
        qipan[i][j] = 1;
        //查看目前棋盘情况
        for (int[] qis : qipan) {
            for (int qi : qis) {
                System.out.print(qi + " ");
            }
            System.out.println();
        }
    }
}
