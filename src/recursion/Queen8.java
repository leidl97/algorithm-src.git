package recursion;

/**
 * @author dali
 * @date 2021-01-28 20:13
 * @Description 八皇后问题，设置为一维数组计算
 */

public class Queen8 {
    //定义一个棋盘,下标表示行，值表示列
    static int[] qipan = new int[8];
    static int count = 0;
    public static void main(String[] args) {
        check(0);
        System.out.println(count);
    }

    //放置第n个皇后
    public static void check(int n) {
        if (n == 8) {
            print();
            System.out.println();
            return;
        }
        for (int i = 0; i < 8; i++) {
            qipan[n] = i;
            if (!judge(n)) {
                //不冲突就跳到下一行
                check(n+1);
            }
        }
    }

    //判断第n个皇后是否造成成图,不需要判断行，斜线用斜率判断,true表示冲突
    public static boolean judge(int n) {
        for (int i = 0; i < n; i++) {
            if (qipan[i] == qipan[n] || Math.abs(n - i) == Math.abs(qipan[i] - qipan[n])) {
                return true;
            }
        }
        return false;
    }

    //打印棋盘的方法
    public static void print() {
        for (int i : qipan) {
            System.out.print("["+ i + "]");
        }
    }
}
