package graph;

/**
 * @author dali
 * @date 2021-07-01 15:35
 * @Description 求每个顶点到所有顶点的最短路径
 */

public class Floyed {
    final int N = 65535;
    int[][] act;
    public static void main(String[] args) {
        //初始化邻接矩阵
        Floyed floyed = new Floyed(7);
        floyed.add(0,1,5);
        floyed.add(0,2,7);
        floyed.add(0,6,2);
        floyed.add(1,3,9);
        floyed.add(1,6,3);
        floyed.add(2,4,8);
        floyed.add(3,5,4);
        floyed.add(4,5,5);
        floyed.add(4,6,4);
        floyed.add(5,6,6);
        print(floyed.act);
        System.out.println("----------");
        floyed.cal(floyed.act);
        print(floyed.act);
    }

    //主方法，每个顶点经过某点到某点依次进行遍历，暴力求解
    private void cal(int[][] act) {
        for (int j = 0; j< act.length; j++) {
            //这层的j表示中间顶点
            for (int i = 0; i< act.length; i++) {
                //这层的i表示起始顶点
                for (int k = 0; k< act.length; k++) {
                    //这层的k表示到达的顶点
                    //距离 = 起始点到中间点的距离 + 中间点到终点的距离
                    int len = act[i][j] + act[j][k];
                    //如果比数组中的值小则进行替换
                    if (len < act[i][k]) {
                        act[i][k] = len;
                    }
                }
            }
        }
    }

    public Floyed(int n) {
        this.act = new int[n][n];
        //先填充为无限远距离
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                this.act[i][j] = N;
            }
        }
        //自身对自身为0
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if (i == j) {
                    this.act[i][j] = 0;
                }
            }
        }
    }

    //构建邻接矩阵
    private void add(int x, int y, int val) {
        this.act[x][y] = val;
        this.act[y][x] = val;
    }

    //打印邻接矩阵
    private static void print(int[][] act) {
        for (int[] ns : act) {
            for (int i : ns) {
                System.out.print(i + "\t");
            }
            System.out.println();
        }
    }
}
