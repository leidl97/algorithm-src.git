package graph;

import java.util.*;

/**
 * @author dali
 * @date 2021-06-22 10:38
 * @Description 普利姆算法---求最小生成树（以顶点生成）
 */

public class Prim {
    public static final int MAX = 65535;
    //构建邻接矩阵
    static int[][] act = new int[7][7];
    //存放终点的数组
    static int[] end = new int[7];
    public static void main(String[] args) {
        Prim prim = new Prim();
        //矩阵初始化
        prim.add(0,1,5);
        prim.add(0,2,7);
        prim.add(0,6,2);
        prim.add(1,3,9);
        prim.add(1,6,3);
        prim.add(2,4,8);
        prim.add(3,5,4);
        prim.add(4,5,5);
        prim.add(4,6,4);
        prim.add(5,6,6);
        prim.print();
        //从A遍历
        Map<String, Integer> map = prim.cal(act, 0);
        System.out.println("该图的最小生成树构成的边为");
        int sum = 0;
        for (Map.Entry<String, Integer> entry: map.entrySet()) {
            String k = entry.getKey();
            int v = entry.getValue();
            String[] s = k.split(":");
            String s1 = getVal(s[0]);
            String s2 = getVal(s[1]);
            System.out.println("边<"+s1+s2+">，长度为"+v);
            sum += v;
        }
        System.out.println("总权值为:"+sum);
    }

    //构建邻接矩阵
    private void add(int x, int y, int val) {
        this.act[x][y] = val;
        this.act[y][x] = val;
    }

    //打印邻接矩阵
    private void print() {
        for (int[] ns : this.act) {
            for (int i : ns) {
                System.out.print(i + "\t");
            }
            System.out.println();
        }
    }

    //生成方法，目标数组，起点
    private Map<String,Integer> cal(int[][] act, int start) {
        //存权值
        Map<String,Integer> map = new HashMap<>();
        //存备选边全部结果集
        Map<String,Integer> allMap = new HashMap<>();
        //存顶点
        List<Integer> list = new ArrayList();
        //顶点集合加入一个起点值
        list.add(start);
        //定义值
        int min = MAX,tempi = 0,tempj = 0;
        //遍历
        while (list.size() < act.length) {
            //获得此次生成的图的最小权值的边是哪条
            for (int i : list) {
                for (int j = 0; j < act.length; j++) {
                    if (act[i][j] != 0 && act[i][j] < min && !this.isExist(i,j,allMap)) {
                        //说明顶点处有边
                        min = act[i][j];
                        tempi = i;
                        tempj = j;
                    }
                }
            }
            //判断该备选边构建的图是否会构成环，不构成则加入结果集
            if (!isValid(tempi, tempj)) {
                this.put(tempi,tempj,min,map,list);
            }
            //备选边如果构成了环不进行处理的话，会一直进行判断该备选边
            allMap.put(tempi + ":" + tempj,min);
            //初始化
            tempi = 0;
            tempj = 0;
            min = MAX;
        }
        return map;
    }

    //判断是否已经加入了该边
    private boolean isExist(int i, int j, Map<String,Integer> map) {
        //是否已经存入包含该最小值
        return map.containsKey(i + ":" + j) || map.containsKey(j + ":" + i);
    }

    //是否构成了环
    public static boolean isValid(int i, int j) {
        //比如边BG 传入值为1，6或者6，1都代表的是一条边，这里判断默认前顶点比后顶点的值小，只取1，6
        //所以当为6，1的情况时，应当做i,j值的调换
        if (i > j) {
            int temp = i;
            i = j;
            j = temp;
        }
        boolean b = false;
        int m = getEnd(i,end);
        int n = getEnd(j,end);
        if (m == n) {
            //构成了环
            b = true;
        } else {
            //否则构建终点数组
            end [m] = n;
        }
        return b;
    }

    //传入顶点，返回该顶点的终点
    private static int getEnd(int i, int[] end) {
        while (end[i] != 0) {
            i = end[i];
        }
        return i;
    }

    //加入顶点集合，用于判断while的结束条件
    private void put(int i, int j, int min, Map<String,Integer> map, List<Integer> list) {
        //加入顶点集合
        if (!list.contains(i)) {
            list.add(i);
        }
        if (!list.contains(j)) {
            list.add(j);
        }
        //加入权值map
        map.put(i + ":" + j, min);
    }

    //将坐标值转为边名称
    public static String getVal(String str) {
        int i = Integer.valueOf(str);
        String s;
        switch (i) {
            case  0 :
                s = "A";
                break;
            case  1 :
                s = "B";
                break;
            case  2 :
                s = "C";
                break;
            case  3 :
                s = "D";
                break;
            case  4 :
                s = "E";
                break;
            case  5 :
                s = "F";
                break;
            case  6 :
                s = "G";
                break;
             default:
                s = "越界";
        }
        return s;
    }
}
