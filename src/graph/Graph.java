package graph;

import java.util.LinkedList;
import java.util.Queue;

/**
 * @author dali
 * @date 2021-04-06 15:30
 * @Description 图
 */

public class Graph {
    int[][] matrix;
    //用过的标记
    boolean[] used;


    //初始化
    public Graph(int n) {
        matrix = new int[n][n];
        used = new boolean[n];
    }

    //创建一个邻接矩阵
    void add(int x, int y) {
        this.matrix[x][y] = 1;
        this.matrix[y][x] = 1;
    }

    //打印邻接矩阵
    void print() {
        for (int[] ns : this.matrix) {
            for (int i : ns) {
                System.out.print(i + "\t");
            }
            System.out.println();
        }
    }

    void dfs(int i) {
        //先输出当前行下标
        System.out.print(i + "\t");
        //将该顶点置为已使用状态
        this.used[i] = true;
        for (int j = 0; j < this.matrix[i].length; j++) {
            if (this.matrix[i][j] == 1 && this.used[j] == false) {
                this.dfs(j);
            }
        }
    }

    void bfs(int i) {
        Queue<Integer> queue = new LinkedList<>();
        queue.offer(i);
        this.used[i] = true;
        System.out.println("bfs遍历结果为");
        System.out.print(i + "\t");
        while (!queue.isEmpty()) {
            //获得队列头下标
            int head = queue.poll();
            for (int j = 0; j < this.matrix[head].length; j++) {
                if (this.matrix[head][j] == 0) {
                    if (this.used[j] == false) {
                        //如果结果为0并且没使用过，说明通过该顶点走不通，跳出循环，换队列中下个顶点继续
                        break;
                    }
                    //如果为0但使用过，说明该顶点已输出，则遍历下一位数
                    continue;
                }
                //如果结果为1并且没有使用过，说明该顶点可以到达该边，输出并置为已使用
                if (this.used[j] == false) {
                    queue.offer(j);
                    System.out.print(j + "\t");
                    this.used[j] = true;
                    continue;
                }
            }
        }
    }

    public static void main(String[] args) {
        Graph graph = new Graph(6);
        //创建一个邻接矩阵
        graph.add(0,2);
        graph.add(0,1);
        graph.add(0,5);
        graph.add(1,0);
        graph.add(1,2);
        graph.add(2,0);
        graph.add(2,1);
        graph.add(2,3);
        graph.add(2,4);
        graph.add(3,5);
        graph.add(3,4);
        graph.add(3,2);
        graph.add(4,3);
        graph.add(4,2);
        graph.add(5,3);
        graph.add(5,0);
        graph.print();
//        System.out.println("dfs遍历结果为");
//        graph.dfs(0);
        graph.bfs(0);
    }

}
