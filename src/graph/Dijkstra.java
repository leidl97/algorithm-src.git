package graph;

import java.util.*;

/**
 * @author dali
 * @date 2021-06-28 14:04
 * @Description 迪杰斯特拉
 */

public class Dijkstra {
    public final int N = 65535;
    //邻接矩阵
    int[][] act;
    //存放前驱结点,主要用于存放路径，可以不用
    int[] pre;
    public static void main(String[] args) {
        //初始化数组，构建图
        Dijkstra dijkstra = new Dijkstra(7);
        dijkstra.add(0,1,5);
        dijkstra.add(0,2,7);
        dijkstra.add(0,6,2);
        dijkstra.add(1,3,9);
        dijkstra.add(1,6,3);
        dijkstra.add(2,4,8);
        dijkstra.add(3,5,4);
        dijkstra.add(4,5,5);
        dijkstra.add(4,6,4);
        dijkstra.add(5,6,6);
//        dijkstra.addV(0,1,2);
//        dijkstra.addV(0,4,10);
//        dijkstra.addV(1,4,4);
//        dijkstra.addV(1,2,3);
//        dijkstra.addV(2,3,4);
//        dijkstra.addV(2,0,4);
//        dijkstra.addV(3,4,5);
//        dijkstra.addV(4,2,3);
//        dijkstra.addV(5,1,3);

        dijkstra.print();
//        dijkstra.cal(dijkstra.getQueue(dijkstra.act,0));
//        System.out.println("queue的结果为："+dijkstra.getQueue(dijkstra.act,0));
//        System.out.println("dis的结果为："+Arrays.toString(dijkstra.dis));
//        System.out.println("pre的结果为："+Arrays.toString(dijkstra.pre));
        System.out.println(Arrays.toString(dijkstra.newCal(0)));
    }

    //采用邻近顶点的方式遍历,n表示从哪个顶点遍历
    public int[] newCal(int n) {
        //使用过的顶点，1代表已使用
        int[] used = new int[act.length];
        used[n] = 1;
        //初始化dis数组
        int[] dis = getDis(n);
        for(int i = 0; i < act.length-2; i++) {
            int temp = getMin(dis, used);
            //遍历n-2次就可以了，初始化的时候遍历过一次，最后一次其余顶点都遍历过了没有意义
            for (int j = 0; j < act.length; j++) {
                //当前的路径值 = 遍历顶点到temp顶点的最优值（比如GA）+ temp到i顶点的值（比如AB）
                int len = dis[temp] + act[temp][j];
                if (used[j] == 0 && len < dis[j]){
                    //如果比当前dis中的小，则更新
                    dis[j] = len;
                }
            }
            //将该顶点置为已使用
            used[temp] = 1;
        }
        return dis;
    }

    //获得dis数组,n表示顶点
    public int[] getDis(int n) {
        int dis[] = new int[act.length];
        for (int i = 0; i< dis.length; i++) {
            dis[i] = act[n][i];
        }
        return dis;
    }

    //获得下一个顶点
    public int getMin(int[] dis,  int[] used) {
        int min = N;
        int result = -1;
        for (int i = 0; i < dis.length; i++) {
            if (used[i] == 1) {
                continue;
            }
            if (dis[i] < min) {
                min = dis[i];
                result = i;
            }
        }
        return result;
    }

    //主方法,传入一个数组和遍历的顶点，这个写法有缺陷
    @Deprecated
    private void cal(Queue<Integer> queue) {
        //初始化dis
        int[] dis = new int[act.length];
        Arrays.fill(dis, N);
        dis[queue.peek()] = 0;
        //初始化前驱节点
        pre = new int[act.length];
        Arrays.fill(pre, queue.peek());

        while (!queue.isEmpty()) {
            int temp = queue.poll();
            for (int i = 0; i < act[temp].length; i++) {
                //顶点的当前值 = 遍历顶点到当前顶点的距离 + 当前顶点到i顶点的距离
                //比如GA = GG + GA = 0 + 2 = 2
                int len = dis[temp] + act[temp][i];
                if (len < dis[i]) {
                    //该顶点未访问过且当前值小于dis之前的值，更新数组中的值
                    dis[i] = len;
                    pre[i] = temp;
                }
            }
        }
    }

    //通过广度优先算法来得到遍历该图的顺序,n表示从哪一点遍历
    public Queue<Integer> getQueue(int[][] act,int n) {
        Queue<Integer> queue = new LinkedList<>();
        Queue<Integer> result = new LinkedList<>();
        int[] used = new int[act.length];
        queue.offer(n);
        while (!queue.isEmpty()) {
            int temp = queue.poll();
            result.offer(temp);
            used[temp] = 1;
            for (int j = 0; j < act[temp].length; j++) {
                if (used[j] == 1) {
                    //如果遍历过该顶点，则跳过
                    continue;
                } else if (act[temp][j] != 0 && act[temp][j] != N) {
                    //表示该两点连通，存入队列，并将该顶点置为已使用
                    queue.offer(j);
                    used[j] = 1;
                } else {
                    //条件都不满足，跳出for
                    break;
                }
            }
        }
        return result;
    }

    //n表示顶点个数
    public Dijkstra(int n) {
        this.act = new int[n][n];
        //先填充为无限远距离
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                this.act[i][j] = N;
            }
        }
//        Arrays.fill(this.act,MAX);
        //自身对自身为0
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if (i == j) {
                    this.act[i][j] = 0;
                }
            }
        }
    }

    //构建邻接矩阵
    private void add(int x, int y, int val) {
        this.act[x][y] = val;
        this.act[y][x] = val;
    }

    //构建邻接矩阵，有向图
    private void addV(int x, int y, int val) {
        this.act[x][y] = val;
    }

    //打印邻接矩阵
    private void print() {
        for (int[] ns : this.act) {
            for (int i : ns) {
                System.out.print(i + "\t");
            }
            System.out.println();
        }
    }
}
