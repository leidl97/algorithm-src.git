package graph;

import java.util.HashMap;
import java.util.Map;

/**
 * @author dali
 * @date 2021-06-23 13:50
 * @Description 克鲁斯卡尔算法,以边生成
 */

public class KrusKal {
    static int[][] act = new int[7][7];
    //存放终点的数组
    static int[] end = new int[7];
    public static void main(String[] args) {
        KrusKal krusKal = new KrusKal();
        krusKal.add(0,1,12);
        krusKal.add(0,5,16);
        krusKal.add(0,6,14);
        krusKal.add(1,2,10);
        krusKal.add(1,5,7);
        krusKal.add(2,3,3);
        krusKal.add(2,4,5);
        krusKal.add(2,5,6);
        krusKal.add(3,4,4);
        krusKal.add(4,5,2);
        krusKal.add(4,6,8);
        krusKal.add(5,6,9);
        krusKal.print();
        Map<String, Integer> map = krusKal.cal(act);
        System.out.println("该图的最小生成树构成的边为");
        int sum = 0;
        for (Map.Entry<String, Integer> entry: map.entrySet()) {
            String k = entry.getKey();
            int v = entry.getValue();
            String[] s = k.split(":");
            String s1 = Prim.getVal(s[0]);
            String s2 = Prim.getVal(s[1]);
            System.out.println("边<"+s1+s2+">，长度为"+v);
            sum += v;
        }
        System.out.println("总权值为:"+sum);
    }

    //主方法
    private Map<String,Integer> cal(int[][] act) {
        //最小生成树结果集合
        Map<String,Integer> map = new HashMap<>();
        //备选边全部集合（包含可能构成环的）
        Map<String,Integer> allMap = new HashMap<>();
        int min = 65535,tempi = 0,tempj = 0;
        while (map.size() < act.length-1) {
            for (int i = 0; i < act.length; i++) {
                 for (int j = i+1; j < act.length; j++) {
                     int temp = act[i][j];
                     if (temp > 0 && temp < min && !isExist(i, j, allMap)) {
                         tempi = i;
                         tempj = j;
                         min = temp;
                     }
                 }
            }
            //判断该备选边构建的图是否会构成环，不构成则加入结果集
            if (!Prim.isValid(tempi, tempj)) {
                map.put(tempi + ":" + tempj,min);
            }
            //备选边如果构成了环不进行处理的话，会一直进行判断该备选边
            allMap.put(tempi + ":" + tempj,min);
            tempi = 0;
            tempj = 0;
            min = 65535;
        }
        return map;
    }

    private boolean isExist(int i, int j, Map<String,Integer> map) {
        //判断集合中是否存在
        return map.containsKey(i + ":" + j);
    }

    //构建邻接矩阵
    private void add(int x, int y, int val) {
        this.act[x][y] = val;
        this.act[y][x] = val;
    }

    //打印邻接矩阵
    private void print() {
        for (int[] ns : this.act) {
            for (int i : ns) {
                System.out.print(i + "\t");
            }
            System.out.println();
        }
    }
}
