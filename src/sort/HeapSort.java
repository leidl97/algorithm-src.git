package sort;

import java.util.Arrays;
import java.util.logging.Level;

/**
 * @author dali
 * @date 2021-03-20 18:11
 * @Description 堆排序
 */

public class HeapSort {
    public static void main(String[] args) {
        int[] a = {4,6,8,5,9};
        System.out.println("排序前:"+ Arrays.toString(a));
//        sort(a);
        adjustSort(a);
        System.out.println("排序后:"+ Arrays.toString(a));
    }

    private static void adjustSort(int[] a) {
        //构建大顶堆
        for (int j = a.length; j > 1; j--) {
            for (int i = a.length / 2 - 1; i >= 0; i--) {
                adjustHeap(a, i, j);
            }
            //将堆顶元素与堆底互换
            int temp = a[0];
            a[0] = a[j - 1];
            a[j - 1] = temp;
        }
    }

    /**
     * 构建大顶堆（升序）
     * @param a 待处理的数组
     * @param i 待处理非叶子节点
     * @param length 待处理数组的长度
     */
    private static void adjustHeap(int[] a, int i, int length) {
        int temp = a[i];
        for (int k = 2 * i + 1; k < length; k = 2 * i + 1) {
            if (k + 1 < length && a[k] < a[k+1]) {
                k++;
            }
            if (a[k] > temp) {
                a[i] = a[k];
                i = k;
            } else {
                //如果当前节点不大于索引节点，那么大顶堆已形成，因为是从下之上的
                break;
            }
        }
        a[i] = temp;
    }

    /**
     * 构建大顶堆（升序）
     * @param a 待处理的数组
     * @param i 待处理非叶子节点
     * @param length 待处理数组的长度
     */
    private static void heapSort(int[] a, int i, int length) {
        while (i >= 0) {
            //从该节点的索引下进行调整，将i节点之下构建为一个大顶堆
            for (int j = 2 * i + 1; j <= length; j = j * 2 + 1) {
                //此时j为左子树
                if (j+1 <= length && a[j] < a[j+1]) {
                    //若左子树大于右子树，则挪动指针，指向右子树
                    j++;
                }
                if (a[i] < a[j]) {
                    //将大元素放置顶堆
                    int temp = a[i];
                    a[i] = a[j];
                    a[j] = temp;
                }
            }
            i--;
        }
    }

    private static void sort(int[] a) {
        //构建大顶堆
        for (int length = a.length - 1; length > 0; length--) {
            heapSort(a, length / 2 - 1, length);
            //将堆顶堆底元素互换
            int temp = a[0];
            a[0] = a[length];
            a[length] = temp;
        }
    }

}
