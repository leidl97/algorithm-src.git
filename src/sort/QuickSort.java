package sort;

import java.util.Arrays;

/**
 * @author dali
 * @date 2021-03-03 18:18
 * @Description 快速排序
 */

public class QuickSort {

    public static void main(String[] args) {
        int[] a = {-9,67,78,0,-1,23,-567,70,34};
        System.out.println("排序前:"+ Arrays.toString(a));
        sort(0, a.length-1,a);
        System.out.println("排序后:"+ Arrays.toString(a));
    }

    private static void sort(int left, int right, int[] arr) {
        int l = left;
        int r = right;
        int center = arr[(l+r)/2];
        int temp = 0;

        while (l < r) {
            while (arr[l] < center) {
                l += 1;
            }

            while (arr[r] > center) {
                r -= 1;
            }

            if (l >= r) {
                break;
            }

            //交换顺序，指针改变
            temp = arr[l];
            arr[l] = arr[r];
            arr[r] = temp;

            l+=1;
            r-=1;
        }

        if (l == r) {
            l+=1;
            r-=1;
        }

        if (left < r) {
            sort(left,r,arr);
        }

        if (right > l) {
            sort(l,right,arr);
        }
    }
}
