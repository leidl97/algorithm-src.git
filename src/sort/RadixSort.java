package sort;

import java.awt.*;
import java.util.Arrays;

/**
 * @author dali
 * @date 2021-03-09 16:59
 * @Description 基数排序，桶排序的升级
 */

public class RadixSort {
    public static void main(String[] args) {
        int[] a = {53,3,542,748,14,214,154,63,616};
        sort(a);
    }

    private static void sort(int[] arr) {
        int max = arr[0];
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] > max) {
                max = arr[i];
            }
        }
        int size = (max + "").length();
        /**
         * size表示循环的次数，根据最大数的位数决定，n表示当前应该取哪一位数，n为1的时候表示取个位
         * n为10表示取百位，以此类推，每次遍历都创建一个length*10的桶，保证数据不会出错
         * 存的时候是按照行列存储的，但取得时候按照列行取（按照人的思维）
         */
        for (int k = 0, n = 1; k < size; k++, n *= 10) {
            //创建一个二维数组用来表示放置的桶
            int[][] bucket = new int[arr.length][10];
            for (int i = 0; i < arr.length; i++) {
                int temp = arr[i];
                int gewei = temp / n % 10;
                bucket[i][gewei] = temp;
            }
            int t = 0;
            for (int i = 0; i < 10; i++){
                for (int j = 0; j < arr.length; j++){
                    if (bucket[j][i] != 0) {
                        arr[t++] = bucket[j][i];
                    }
                }
            }
            System.out.println(Arrays.toString(arr));
        }
    }

    private static void print(int[][] arr) {
        for (int[] row1 : arr){
            for (int data : row1){
                //按十进制输出+tab
                System.out.printf("%d\t",data);
            }
            System.out.println();
        }
    }
}
