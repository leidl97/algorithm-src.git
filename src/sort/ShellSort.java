package sort;

import java.util.Arrays;

/**
 * @author dali
 * @date 2021-02-23 18:27
 * @Description
 */

public class ShellSort {
    public static void main(String[] args) {
        int[] a = {4,3,5,2,1,9,8,10,7,6};
//        int[] a = {9,6,3,2,1};
//        for (int i = 0; i < a.length; i++) {
//            a[i] = (int)(Math.random()*100);
//        }
        System.out.println("排序前:"+ Arrays.toString(a));
        shellSort(a);
        System.out.println("排序后:"+Arrays.toString(a));
    }

    private static void shellSort(int[] a) {
        int count = 0;
        for (int gap = a.length /2; gap > 0; gap /= 2) {
            for (int i = gap; i < a.length; i++) {
                int index = i;
                int arr = a[index];
                if (arr < a[index-gap]) {
                    while (index - gap >= 0 && arr < a[index-gap]) {
                        count ++ ;
                        a[index] = a[index-gap];
                        index-=gap;
                    }
                    a[index] = arr;
                }
            }
        }
        System.out.println("一共："+count+"次");
    }
}
