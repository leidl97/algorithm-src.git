package sort;

import java.util.Arrays;

/**
 * @author dali
 * @date 2021-02-02 20:05
 * @Description 冒泡排序
 */

public class BubblingSort {
    public static void main(String[] args) {
        int[] a = new int[1000];
        //测试
        for (int i = 0; i < a.length; i++) {
            a[i] = (int)(Math.random()*10000);
        }
        System.out.println("排序前:"+Arrays.toString(a));
        sort(a);
        System.out.println("排序后:"+Arrays.toString(a));
    }

    //冒泡排序
    private static void sort(int[] a) {
        //只能用于判断趟数，省趟数，不省次数
        boolean flag = true;
        for (int i = 0; i < a.length - 1; i++) {
            //排序n - 1趟
            for (int j = 0; j < a.length - 1 - i; j++) {
                if (a[j] > a[j+1]) {
                    swag(a,j,j+1);
                    flag = false;
                }
//                System.out.println("第"+(i+1)+"趟第"+(j+1)+"次排序完成");
//                System.out.println(Arrays.toString(a));
            }
            if (flag) {
                break;
            } else {
                //不重置只要交换过就没用了
                flag = true;
            }
        }
    }

    //数组下标i和j的数据交换
    private static void swag(int[] a, int i, int j) {
        int k = 0;
        k = a[i];
        a[i] = a[j];
        a[j] = k;
    }

}
