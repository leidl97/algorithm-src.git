package sort;

import java.util.Arrays;

/**
 * @author dali
 * @date 2021-02-03 19:46
 * @Description 插入排序
 */

public class InsertSort {
    public static void main(String[] args) {
        int[] a = new int[5];
        //测试
        for (int i = 0; i < a.length; i++) {
            a[i] = (int)(Math.random()*100);
        }
//        a[0] = 2 ;
//        a[1] = 4;
//        a[2] = 5;
//        a[3] = 3;
//        a[4] = 1;
        System.out.println("排序前:"+ Arrays.toString(a));
        sort(a);
        System.out.println("排序后:"+Arrays.toString(a));
    }

    private static void sort(int[] a) {
        for (int i = 0, k= i+1; i < a.length-1; i++,k++) {
            for (int j = i+1; j > 0; j--) {
                if (a[k] > a[j-1]) {
                    swag(a,j,k);
                    break;
                }
                if (j - 1 == 0) {
                    all(a,k);
                    break;
                }
            }
        }
    }

    //数组下标i和j的数据交换
    private static void swag(int[] a, int i, int j) {
        int k = a[j];
        int size = j-i;
        for (int b = 0; b < size; b++,j--) {
            a[j] = a[j-1];
        }
        a[i] = k;
    }

    //全排序
    private static void all(int[] a, int j) {
        int k = a[j];
        int size = j;
        for (int b = 0; b < size; b++,j--) {
            a[j] = a[j-1];
        }
        a[0] = k;
    }
}
