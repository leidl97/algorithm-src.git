package sort;

import java.util.Arrays;

/**
 * @author dali
 * @date 2021-02-04 20:50
 * @Description 插入排序精简版
 */

public class InsertSort2 {
    public static void main(String[] args) {
        int[] a = {4,3,5,2,1,9,8,10,7,6};
//        for (int i = 0; i < a.length; i++) {
//            a[i] = (int)(Math.random()*100);
//        }
        System.out.println("排序前:"+ Arrays.toString(a));
        sort(a);
        System.out.println("排序后:"+Arrays.toString(a));
    }

    private static void sort(int[] a) {
        int count = 0;
        for (int i = 0; i < a.length-1; i++) {
            int arr = a[i+1];
            int index = i;
            while (index >= 0 && arr < a[index]) {
                count ++ ;
                a[index + 1] = a[index];
                index--;
            }
            a[index+1] = arr;
        }
        System.out.println("一共："+count+"次");
    }
}
