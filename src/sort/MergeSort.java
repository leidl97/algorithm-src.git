package sort;

import java.util.Arrays;

/**
 * @author dali
 * @date 2021-03-05 10:08
 * @Description 归并排序，分而治之
 */

public class MergeSort {
    static int count = 0;
    public static void main(String[] args) {
        int[] a = {8,4,5,7,1,3,6,2};
        sort(a,0,a.length-1);
//        int[] a = {4,5,7,8,1,2,3,6};
//        merge(a,0,3,7);
        System.out.println("排序后："+Arrays.toString(a));
        System.out.println(count);
    }

    private static void sort(int[] arr, int left, int right) {
        if (left < right) {
            int mid = (left + right) / 2;
            //左归并
            sort(arr, left, mid);
            //右归并
            sort(arr, mid + 1, right);
            //归并计算
            merge(arr, left, mid, right);
        }
    }

    private static void merge(int[] arr, int left, int mid, int right) {
        count ++ ;
        int i = left;
        int j = mid + 1;
        int[] temp = new int[right-left+1];
        int t = 0;

        while (i <= mid && j <= right) {
            if (arr[i] < arr[j]) {
                //左边小，将左边的数放入数组中
                temp[t++] = arr[i++];
            }else {
                //右边小，将右边的数据放入数组中
                temp[t++] = arr[j++];
            }
        }

        //将剩下的数据放入数组中
        while (i <= mid) {
            temp[t++] = arr[i++];
        }

        while (j <= right) {
            temp[t++] = arr[j++];
        }

        //将temp的数据放入arr中
        t = 0;
        while (t < temp.length) {
            arr[left++] = temp[t++];
        }
    }

}
