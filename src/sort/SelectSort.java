package sort;

import java.util.Arrays;

/**
 * @author dali
 * @date 2021-02-03 18:53
 * @Description 选择排序
 */

public class SelectSort {
    public static void main(String[] args) {
        int[] a = new int[5];
        //测试
        for (int i = 0; i < a.length; i++) {
            a[i] = (int)(Math.random()*100);
        }
        System.out.println("排序前:"+Arrays.toString(a));
        sort(a);
        System.out.println("排序后:"+Arrays.toString(a));
    }

    private static void sort(int[] a) {
        for (int i = 0; i < a.length-1; i++) {
            for (int j = i; j < a.length-1; j++) {
                if (a[j+1] < a[i]) {
                    swag(a,i,j+1);
                }
            }
        }
    }

    //数组下标i和j的数据交换
    private static void swag(int[] a, int i, int j) {
        int k = 0;
        k = a[i];
        a[i] = a[j];
        a[j] = k;
    }
}
