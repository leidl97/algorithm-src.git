package tree;

/**
 * @author dali
 * @date 2021-03-31 15:26
 * @Description 2-3树的构建
 */

public class BTree {
    int min;
    int max;
    BTree leftTree;
    BTree centerTree;
    BTree rightTree;

    /**
     * 利用数组给定的数值构建一颗2-3树
     * 必要的规则如下
     * 1、所有叶子节点必须在同一层（也是B树的条件）
     * 2、二三节点必须为满节点，或者没有节点
     * 3、当插入一个数不满足上述规则，就需要拆，先向上拆，如果满了拆本层，直到满足条件
     * 4、数值还需要满足BST的规则
     * @param a
     */
    public void addBTree(int[] a) {

    }

    public static void main(String[] args) {
        int[] a = {16,24,12,32,14,26,34,10,8,28,38,20};
    }
}
