package tree;

import java.lang.reflect.Modifier;

/**
 * @author dali
 * @date 2021-03-29 17:03
 * @Description 平衡二叉树
 */

public class BalanceTree {

    //当前结点的左子树高度
    static int leftHeight = 0;
    //当前结点的右子树高度
    static int rightHeight = 0;

    public static void main(String[] args) {
//        int[] a = {4,2,6,5,7,8};
        int[] a = {10,11,7,6,8,9};
        Node node = buildBalanceTree(a);
//        int[] a = {6,3,7,4,2,1};
//        Node node = buildBalanceTree(a);
        Node.centerSort(node);
    }

    //构建一颗平衡二叉树
    public static Node buildBalanceTree(int[] a) {
        Node node = new Node(a[0]);
        int status;
        for (int i = 1; i < a.length; i++) {
            int temp = a[i];
            Node.findNullPoint(temp, node);
            //对不满足平衡二叉树的条件进行旋转
            turn(node);
        }
        return node;
    }

    //进行旋转
    private static void turn(Node node) {
        int status = isCheckNode(node);
        if (status == -1) {
            //进行右旋
            if (getHeight(node.leftNode.rightNode) > getHeight(node.rightNode)) {
                //进行局部旋转
                turnLeft(node.leftNode);
            }
            turnRight(node);
        }
        if (status == 1) {
            //进行左旋
            if (isCheckNode(node.rightNode.leftNode) > isCheckNode(node.leftNode)) {
                //进行局部旋转
                turnRight(node.rightNode);
            }
            turnLeft(node);
        }
    }

    /**
     * 进行右旋
     * @param node
     */
    private static void turnRight(Node node) {
        //1、创建新节点，值与当前结点相同
        Node temp = new Node(node.value);
        //2、将新节点右子树指向当前结点的右子树
        temp.rightNode = node.rightNode;
        //3、将新节点的左子树指向当前结点的左子树的右子树
        temp.leftNode = node.leftNode.rightNode;
        //4、将当前结点的值变为当前结点的左子树结点的值
        node.value = node.leftNode.value;
        //5、将当前结点的右子树指向新节点
        node.rightNode = temp;
        //6、将当前结点的左子树指向当前结点左子树的左子树
        node.leftNode = node.leftNode.leftNode;
    }

    /**
     * 进行左旋
     * @param node
     */
    private static void turnLeft(Node node) {
        //1、创建新节点，值与当前结点相同
        Node temp = new Node(node.value);
        //2、将新节点左子树指向当前结点的左子树
        temp.leftNode = node.leftNode;
        //3、将新节点的右子树指向当前结点的右子树的左子树
        temp.rightNode = node.rightNode.leftNode;
        //4、将当前结点的值变为当前结点的右子树结点的值
        node.value = node.rightNode.value;
        //5、将当前结点的左子树指向新节点
        node.leftNode = temp;
        //6、将当前结点的右子树指向当前结点右子树的右子树
        node.rightNode = node.rightNode.rightNode;
    }

    /**
     * 检查该树的状态 0 表示平衡二叉树
     * -1表示左子树高
     * 1表示右子树高
     * @param node
     * @return
     */
    private static int isCheckNode(Node node) {
        int lh = getLeftHeight(node);
        int rh = getRightHeight(node);
        if (lh > rh && lh - rh > 1) {
            return -1;
        }
        if(rh > lh && rh -lh > 1) {
            return 1;
        }
        return 0;
    }

    /**
     * 左子树高度
     * @param node
     */
    private static int getLeftHeight(Node node) {
        if (node.leftNode == null) {
            return 0;
        }
        return getHeight(node.leftNode);
    }

    private static int getHeight(Node node) {
        return Math.max(node.leftNode == null ? 0 : getHeight(node.leftNode), node.rightNode == null ? 0 : getHeight(node.rightNode)) + 1;
    }

    /**
     * 右子树高度
     * @param node
     */
    private static int getRightHeight(Node node) {
        if (node.rightNode == null) {
            return 0;
        }
        return getHeight(node.rightNode);
    }

}
