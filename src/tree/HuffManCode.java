package tree;

import java.util.*;

/**
 * @author dali
 * @date 2021-03-24 15:34
 * @Description 赫夫曼编码
 */

public class HuffManCode {
    //设置编码集
    static Map<Byte,String> results = new HashMap<>();
    public static void main(String[] args) {
        String s = "i like like like java do you like a java";
        List<Byte> encoding = encoding(s);
        if (encoding == null) {
            System.out.println("待加密文本为单一字符，无法进行编码");
            return;
        }
        System.out.println("压缩前该文本长度为："+s.length());
        System.out.println("压缩前该文本长度为："+encoding.size()+"。压缩率为："+ (Double.valueOf(s.length() - encoding.size()) / Double.valueOf(s.length()) * 100) + "%");
        String result = decoding(encoding, results);
        System.out.println("解码后的文本为："+result);
    }

    /**
     * 赫夫曼解码
     * @param bytes 需要解密的文本
     * @param results 编码集
     */
    private static String decoding(List<Byte> bytes, Map<Byte,String> results) {
        //根据编码集results创建解码集
        Map<String, Byte> decoding = new HashMap<>();
        results.forEach((k,v) -> decoding.put(v,k));
        //将给定的byte编码集先转为8位2进制字符串
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < bytes.size(); i++) {
            byte b = bytes.get(i);
            //返回的为二进制的补码
            String string = Integer.toBinaryString(b);
            if (b < 0) {
                string = string.substring(string.length() - 8);
            }
            //表示8位整数，不够高位补0，最后一位且为正数不做处理
            if (i != bytes.size() - 1 && b > 0) {
                string = String.format("%08d",Integer.parseInt(string));
            }
            sb.append(string);
        }
        String result = sb.toString();
        //创建结果集
        String temp = "";
        List<Byte> byteResults = new ArrayList<>();
        char[] array = result.toCharArray();
        //将得到的二进制串按照解码集去解码得到byte集合
        for (char c : array) {
            temp += c;
            Byte b = decoding.get(temp);
            if (b != null) {
                byteResults.add(b);
                temp = "";
            }
        }
        //将byte集合转为byte数组
        byte[] bs = new byte[byteResults.size()];
        for (int i = 0; i < bs.length; i++) {
            bs[i] = byteResults.get(i);
        }
        return new String(bs);
    }

    //将文本信息转为赫夫曼编码
    private static List<Byte> encoding(String s) {
        byte[] bs = s.getBytes();
        //定义赫夫曼树结点
        List<HuffManTree> nodes = new ArrayList<>();
        //定义已经存在的字符集合
        List<Byte> bytes = new ArrayList<>();
        //定义已经存在的字符出现的次数集合
        Map<Byte,Integer> flags = new HashMap<>();
        //创建构建赫夫曼树所需的结点信息
        nodeBuild(flags,bytes,nodes,bs);
        //利用这些结点构建赫夫曼树
        HuffManTree huffManTree = huffmanBuild(nodes);
        if (huffManTree.left == null && huffManTree.right == null) {
            return null;
        }
        //进行编码
        huffmanEncoding(huffManTree, results,"");
//        results.forEach((k,v) -> System.out.println("数据为："+k+"。对应的编码值为："+v));
        //将字长进行压缩
        return zip(bs,results);
    }

    /**
     * 获取需要构成赫夫曼树的叶子结点
     * @param flags 计算次数
     * @param bytes 放入结果
     * @param nodes 结果集中的结点
     * @param bs 原始数据
     */
    private static void nodeBuild(Map<Byte,Integer> flags, List<Byte> bytes,List<HuffManTree> nodes, byte[] bs) {
        for (byte b : bs) {
            if (bytes.contains(b)) {
                //说明已经存在
                flags.put(b, flags.get(b)+1);
                continue;
            }
            //说明第一次遍历到
            flags.put(b, 1);
            bytes.add(b);
        }

        flags.forEach((k,v) -> nodes.add(new HuffManTree(k,v)));
    }

    private static List<Byte> zip(byte[] bs, Map<Byte,String> results) {
        //替换原有编码
        StringBuffer sb = new StringBuffer();
        //生成新编码
        List<Byte> bytes = new ArrayList<>();
//        System.out.print("原始数据为：");
//        for (byte b : bs) {
//            System.out.print(b + "\t");
//        }
//        System.out.println();
        //将原始byte数组的值替换为对应的编码
        for (Byte b : bs) {
            sb.append(results.get(b));
        }
        String s;
        for (int i = 0; i < sb.length(); i+=8) {
            s = i+8 < sb.length() ? sb.substring(i,i+8) : sb.substring(i,sb.length());
            byte a = (byte)Integer.parseInt(s, 2);
            bytes.add(a);
        }
//        System.out.println(bytes);
        System.out.println(sb);
        System.out.println("该编码的字长为："+sb.length());
        return bytes;
    }

    /**
     * 进行编码
     * @param huffManTree 赫夫曼树
     * @param results 编码值哈希表
     * @param s 记录编码值
     */
    private static void huffmanEncoding(HuffManTree huffManTree, Map<Byte,String> results, String s) {
        //进行遍历，然后处理
        if (huffManTree.b != 0) {
            //当前遍历到的元素不为0，说明为结点元素，需要给一个编码值
            results.put(huffManTree.b, s);
        }
        if (huffManTree.left != null) {
            //这里不能写s += "0"，因为返回上一层已经在上一层先赋值在使用，所以上层会延续下层的值
            huffmanEncoding(huffManTree.left, results, s + "0");
        }
        if (huffManTree.right != null) {
            huffmanEncoding(huffManTree.right, results, s + "1");
        }
    }

    private static HuffManTree huffmanBuild(List<HuffManTree> list) {
        //需要实现Comparable接口并重写对应的方法
        Collections.sort(list);
        while (list.size() > 1) {
            HuffManTree leftNode = list.get(0);
            HuffManTree rigtNode = list.get(1);
            HuffManTree parent = new HuffManTree(leftNode.weight + rigtNode.weight);
            //构建一颗二叉树
            parent.left = leftNode;
            parent.right = rigtNode;
            list.remove(leftNode);
            list.remove(rigtNode);
            list.add(parent);
            //将现有的集合进行排序
            Collections.sort(list);
        }
        //最后剩下的是这颗赫夫曼树的头节点
//        System.out.println("成功构建该树，前序遍历结果如下");
//        preTraversal(list.get(0));
        return list.get(0);
    }

    //前序遍历
    private static void preTraversal(HuffManTree node) {
        if (node != null) {
            System.out.println(node);
        }
        if (node.left != null) {
            preTraversal(node.left);
        }
        if (node.right != null) {
            preTraversal(node.right);
        }
    }
}
