package tree;

/**
 * @author dali
 * @date 2021-03-25 16:47
 * @Description 二叉排序树
 */

public class BinaryTreeSort {
    public static void main(String[] args) {
        int[] a = {7,3,10,12,5,1,9,2};
        Node node = Node.buildTree(a);
        Node.centerSort(node);
        System.out.println();
        Node.delete(node, new Node(3));
        Node.delete(node, new Node(5));
        Node.delete(node, new Node(12));
        Node.centerSort(node);
    }

}