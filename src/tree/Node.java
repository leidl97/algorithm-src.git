package tree;

import java.util.ArrayList;
import java.util.List;

/**
 * @author dali
 * @date 2021-03-16 10:04
 * @Description 二叉树节点定义
 */

public class Node {
    //父节点，暂时用不到
    private Node parentNode;
    //左子树
    public Node leftNode;
    //右子树
    public Node rightNode;
    //节点的权
    public int value;

    public static List<Integer> list = new ArrayList<>();

    public Node() {

    }

    public Node(int value) {
        this.value = value;
    }

    //删除结点,需要考虑叶子结点，有一个子节点，有两个子节点
    public static void delete(Node node, Node deleteNode) {
        if (deleteNode.value == node.value) {
            //说明删除的为根节点，直接返回
            System.out.println("不能删除根节点！");
            return;
        }
        if (deleteNode.value < node.value) {
            if (node.leftNode == null) {
                System.out.println("未找到需要删除的结点");
                return;
            }
            if (node.leftNode.value == deleteNode.value) {
                //相等的就判断结点类型
                if (node.leftNode.leftNode == null && node.leftNode.rightNode == null) {
                    //说明为叶子结点,直接将其置为空
                    node.leftNode = null;
                } else if(node.leftNode.leftNode != null && node.leftNode.rightNode != null) {
                    //说明有两个子节点
                    buildSort(node.leftNode);
                    list.remove(Integer.valueOf(node.leftNode.value));
                    int[] temp = new int[list.size()];
                    for (int i = 0; i < list.size(); i++) {
                        temp[i] = list.get(i);
                    }
                    Node tempNode = buildTree(temp);
                    node.leftNode = tempNode;
                } else if (node.leftNode.leftNode != null) {
                    //只有一个左子节点，将其替换
                    node.leftNode = node.leftNode.leftNode;
                } else {
                    //只有一个右子结点，将其替换
                    node.leftNode = node.leftNode.rightNode;
                }
                return;
            }
            delete(node.leftNode, deleteNode);
            return;
        }
        if (deleteNode.value > node.value) {
            if (node.rightNode == null) {
                System.out.println("未找到需要删除的结点");
                return;
            }
            if (node.rightNode.value == deleteNode.value) {
                //相等的就判断结点类型
                if (node.rightNode.leftNode == null && node.rightNode.rightNode == null) {
                    //说明为叶子结点,直接将其置为空
                    node.rightNode = null;
                } else if(node.rightNode.leftNode != null && node.rightNode.rightNode != null) {
                    //说明有两个子节点
                    buildSort(node.rightNode);
                    list.remove(Integer.valueOf(node.rightNode.value));
                    int[] temp = new int[list.size()];
                    for (int i = 0; i < list.size(); i++) {
                        temp[i] = list.get(i);
                    }
                    Node tempNode = buildTree(temp);
                    node.rightNode = tempNode;
                } else if (node.rightNode.leftNode != null) {
                    //只有一个左子节点，将其替换
                    node.rightNode = node.rightNode.leftNode;
                } else {
                    //只有一个右子结点，将其替换
                    node.rightNode = node.rightNode.rightNode;
                }
                return;
            }
            delete(node.rightNode, deleteNode);
            return;
        }
    }

    public static void centerSort(Node node) {
        //首先找到最左节点
        if (node.getLeftNode()!=null) {
            centerSort(node.getLeftNode());
        }
        //其次输出当前节点
        System.out.print(node.getValue() + "\t");
        //最后向右进行遍历
        if (node.getRightNode()!=null) {
            centerSort(node.getRightNode());
        }
    }

    public static void buildSort(Node node) {
        //首先找到最左节点
        if (node.getLeftNode()!=null) {
            buildSort(node.getLeftNode());
        }
        //其次输出当前节点
        list.add(node.getValue());
        //最后向右进行遍历
        if (node.getRightNode()!=null) {
            buildSort(node.getRightNode());
        }
    }

    public static Node buildTree(int[] a) {
        Node node = new Node(a[0]);
        for (int i = 1; i< a.length; i++) {
            int temp = a[i];
            findNullPoint(temp, node);
        }
        return node;
    }

    //找到空结点
    public static void findNullPoint(int a, Node node) {
        if (a <= node.value) {
            if (node.leftNode == null) {
                node.leftNode = new Node(a);
                return;
            }
            findNullPoint(a, node.leftNode);
        }
        if (a > node.value) {
            if (node.rightNode == null) {
                node.rightNode = new Node(a);
                return;
            }
            findNullPoint(a, node.rightNode);
        }
    }


    public Node getParentNode() {
        return parentNode;
    }

    public void setParentNode(Node parentNode) {
        this.parentNode = parentNode;
    }

    public Node getLeftNode() {
        return leftNode;
    }

    public void setLeftNode(Node leftNode) {
        this.leftNode = leftNode;
    }

    public Node getRightNode() {
        return rightNode;
    }

    public void setRightNode(Node rightNode) {
        this.rightNode = rightNode;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "Node{value=" + value +'}';
    }
}
