package tree;

/**
 * @author dali
 * @date 2021-03-16 10:03
 * @Description 二叉树的定义
 */

public class Tree {
    //根节点
    private Node root;

    public int count = 0;

    public Tree() {
        root = new Node();
    }

    public Tree(Node node) {
        this.root = node;
    }

    public Node getRoot() {
        return root;
    }

    public void setRoot(Node root) {
        this.root = root;
    }

    //前序遍历
    public void beforeTraversal(Node node) {
        //首先输出当前节点
        System.out.print(node.getValue() + "\t");
        //向左遍历
        if (node.getLeftNode()!=null) {
            beforeTraversal(node.getLeftNode());
        }
        //向右遍历
        if (node.getRightNode()!=null) {
            beforeTraversal(node.getRightNode());
        }
    }

    //前序遍历查找
    public void beforeTraversalSearch(Node node, int value) {
        count++;
        if (node.getValue() == value) {
            System.out.println("找到该元素:"+value+"，次数为:"+count);
            return;
        }
        //向左遍历
        if (node.getLeftNode()!=null) {
            beforeTraversalSearch(node.getLeftNode(),value);
        }
        //向右遍历
        if (node.getRightNode()!=null) {
            beforeTraversalSearch(node.getRightNode(),value);
        }
    }

    //中序遍历
    public void centerTraversal(Node node) {
        //首先找到最左节点
        if (node.getLeftNode()!=null) {
            centerTraversal(node.getLeftNode());
        }
        //其次输出当前节点
        System.out.print(node.getValue() + "\t");
        //最后向右进行遍历
        if (node.getRightNode()!=null) {
            centerTraversal(node.getRightNode());
        }
    }

    //后序遍历
    public void afterTraversal(Node node) {
        //首先找到最左节点
        if (node.getLeftNode()!=null) {
            centerTraversal(node.getLeftNode());
        }
        //其次向右遍历
        if (node.getRightNode()!=null) {
            centerTraversal(node.getRightNode());
        }
        //最后输出节点
        System.out.print(node.getValue() + "\t");
    }

    //删除节点
    public void deleteNode(Node node, int value) {
        if (this.root.getLeftNode() == null && this.root.getRightNode() == null) {
            System.out.println("根节点为空");
            return;
        }
        count++;
        //向左遍历
        if (node.getLeftNode()!=null) {
            if (node.getLeftNode().getValue() == value) {
                //找到该节点，删除
                node.setLeftNode(null);
                System.out.println("找到该元素:"+value+"，次数为:"+count);
                System.out.print("当前节点情况为:");
                beforeTraversal(this.root);
                return;
            }
            deleteNode(node.getLeftNode(),value);
        }
        //向右遍历
        if (node.getRightNode()!=null) {
            if (node.getRightNode().getValue() == value) {
                //找到该节点，删除
                node.setRightNode(null);
                System.out.println("找到该元素:"+value+"，次数为:"+count);
                System.out.print("当前节点情况为:");
                beforeTraversal(this.root);
                return;
            }
            deleteNode(node.getRightNode(),value);
        }
    }
}

