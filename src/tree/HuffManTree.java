package tree;

import java.time.temporal.ValueRange;
import java.util.*;

/**
 * @author dali
 * @date 2021-03-24 10:25
 * @Description 赫夫曼树的构建
 */

public class HuffManTree implements Comparable<HuffManTree>{
    //数据
    byte b;
    //权值
    int weight;
    //左子树
    HuffManTree left;
    //右子树
    HuffManTree right;

    public HuffManTree() {

    }

    @Override
    public String toString() {
        return "HuffManTree{" +
                "b=" + b +
                ", weight=" + weight +
                '}';
    }

    public HuffManTree(int weight) {
        this.weight = weight;
    }

    public HuffManTree(byte b, int weight) {
        this.b = b;
        this.weight = weight;
    }
    public static void main(String[] args) {
        int[] a = {13,7,8,3,29,6,1};
//        int[] a = {1,3,6,7,8,13,29};
        huffmanBuild(a);
    }

    private static void huffmanBuild(int[] a) {
        Arrays.sort(a);
        List<Node> nodes = new ArrayList<>();
        for (int value : a) {
            //向集合加入node对象
            nodes.add(new Node(value));
        }
        while (nodes.size() > 1) {
            Node leftNode = nodes.get(0);
            Node rigtNode = nodes.get(1);
            Node parent = new Node(leftNode.getValue() + rigtNode.getValue());
            //构建一颗二叉树
            parent.setLeftNode(leftNode);
            parent.setRightNode(rigtNode);
            nodes.remove(leftNode);
            nodes.remove(rigtNode);
            nodes.add(parent);
            //将现有的集合进行排序
            Collections.sort(nodes, new Comparator<Node>() {
                @Override
                public int compare(Node o1, Node o2) {
                    return o1.getValue() - o2.getValue();
                }
            });
        }
        //最后剩下的是这颗赫夫曼树的头节点
        Tree tree = new Tree(nodes.get(0));
        tree.beforeTraversal(tree.getRoot());
        System.out.println();
    }

    //构建赫夫曼树
    private static void HuffManBuild(int[] a) {
        Arrays.sort(a);
        int value = a[0] + a[1];
        Node[] nodes = new Node[10];
        for (int i = 0; i < nodes.length; i++) {
            nodes[i] = new Node();
        }
        nodes[0].setValue(value);
        nodes[1].setValue(a[0]);
        nodes[2].setValue(a[1]);
        //a0作为左子树
        nodes[0].setLeftNode(nodes[1]);
        nodes[0].setRightNode(nodes[2]);
        nodes[3].setValue(6);
        value = nodes[0].getValue() + a[2];
        nodes[4].setValue(value);
        if (nodes[0].getValue() < a[2]) {
            //则作为左子节点
            nodes[4].setLeftNode(nodes[0]);
            nodes[4].setRightNode(nodes[3]);
        } else if(nodes[0].getValue() > a[3]){
            //大于数组后的两个结点，数组的两个结点自动构建一个二叉树
            value = a[2] + a[3];
            nodes[5].setValue(value);
            nodes[6].setValue(a[2]);
            nodes[7].setValue(a[3]);
            nodes[5].setLeftNode(nodes[6]);
            nodes[5].setRightNode(nodes[7]);
        }
        Tree tree = new Tree();
        tree.setRoot(nodes[4]);
        tree.beforeTraversal(tree.getRoot());

        for (int i = 0; i < a.length; i++) {
            //总共循环数组长度次
            value = a[i] + a[i+1];
            nodes[i].setValue(value);
            nodes[i+1].setValue(a[i]);
            nodes[i+2].setValue(a[i+1]);
            //构建一颗二叉树
            nodes[i].setLeftNode(nodes[i+1]);
            nodes[i].setRightNode(nodes[i+2]);
            if (nodes[i].getValue() < a[i+2]) {

            }
        }
    }

    @Override
    public int compareTo(HuffManTree o) {
        return this.weight - o.weight;
    }
}
