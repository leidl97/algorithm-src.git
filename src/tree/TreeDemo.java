package tree;

/**
 * @author dali
 * @date 2021-03-16 10:30
 * @Description
 */

public class TreeDemo {
    public static void main(String[] args) {
        Tree tree = new Tree();
        Node node1 = new Node();
        Node node2 = new Node();
        Node node3 = new Node();
        Node node4 = new Node();
        node1.setValue(1);
        node2.setValue(2);
        node3.setValue(3);
        node4.setValue(4);
        tree.getRoot().setValue(0);
        tree.getRoot().setLeftNode(node1);
        tree.getRoot().setRightNode(node2);
        node1.setLeftNode(node3);
        node1.setRightNode(node4);
        tree.deleteNode(tree.getRoot(), 2);
    }
}
