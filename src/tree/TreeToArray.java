package tree;

/**
 * @author dali
 * @date 2021-03-17 10:58
 * @Description 顺序存储二叉树转数组
 */

public class TreeToArray {
    public static void main(String[] args) {
        int[] a = {0,1,2,3,4,5,6};
//        new TreeToArray().ArrToTree(a);
        new TreeToArray().preOrder(a,0);
    }

    public Tree ArrToTree(int[] a) {
        Node[] nodes = new Node[7];
        for (int i = 0; i< nodes.length; i++) {
            nodes[i] = new Node();
        }
        //开始遍历插入值
        nodes[0].setValue(0);
        insert(a, nodes, 0);
        Tree tree = new Tree();
        tree.setRoot(nodes[0]);

        for (int i = 0; i < 7; i++) {
            System.out.print(nodes[i].getValue() + "\t");
        }
        return tree;
    }

    //index为数组下标
    public void preOrder(int[] a, int index) {
        if (a == null || a.length == 0) {
            System.out.println("数组为空，无需遍历");
            return;
        }
        System.out.println(a[index]);
        if ((2*index + 1) < a.length) {
            //向左遍历
            preOrder(a, 2*index + 1);
        }
        if ((2*index + 2) < a.length) {
            //向右遍历
            preOrder(a, 2*index + 2);
        }
    }

    public void insert(int[] a,Node[] nodes, int i) {
        String s = a.length + 1 + "";
        double b = Math.log(Double.valueOf(s)) / Math.log(2);
        if (i < b) {
            nodes[i].setLeftNode(nodes[2*i+1]);
            nodes[i].getLeftNode().setValue(a[2*i+1]);
            nodes[i].setRightNode(nodes[2*i+2]);
            nodes[i].getRightNode().setValue(a[2*i+2]);
            insert(a, nodes, i+1);
        }
    }
}
