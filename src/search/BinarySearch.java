package search;

import java.util.ArrayList;
import java.util.List;

/**
 * @author dali
 * @date 2021-03-10 13:40
 * @Description 二分查找，可以查找相同数据
 */

public class BinarySearch {
    static List<Integer> list = new ArrayList();
    public static void main(String[] args) {
        int[] a = {1,2,3,8,9,10};
//        search2(a,0,a.length-1,3);
//        System.out.println("找到的数据有"+list.size()+"个");
        search2(a,0,a.length-1,4);
//        search3(a, 4);
    }

    private static void search(int[] arr,int left, int right, int act) {
        if (left <= right) {
            int mid = (left + right) / 2;
            if (arr[mid] > act) {
                search(arr, left, mid, act);
            } else if (arr[mid] < act) {
               search(arr,mid+1, right, act);
            } else {
                System.out.println("找到数据");
                list.add(arr[mid]);
                int i = mid+1;
                if (i < arr.length) {
                    while (arr[i++] == arr[mid]) {
                        System.out.println("找到数据");
                        list.add(arr[mid]);
                    }
                }
                int j = mid-1;
                if (j > 0) {
                    while (arr[j--] == arr[mid]) {
                        System.out.println("找到数据");
                        list.add(arr[mid]);
                    }
                }
                System.out.println("找到的数据有"+list.size()+"个");
            }
        } else {
            System.out.println("没有该数据");
        }
    }

    //递归实现
    private static void search2(int[] arr,int left, int right, int act) {
        if (left <= right) {
            int mid = (left + right) / 2;
            if (arr[mid] > act) {
                search2(arr, left, mid-1, act);
            } else if (arr[mid] < act) {
                search2(arr,mid+1, right, act);
            } else {
                System.out.println("找到数据");
                list.add(arr[mid]);
                search2(arr, left, mid-1, act);
                search2(arr,mid+1, right, act);
            }
        } else {
            System.out.println("没有该数据");
            return;
        }
    }

    //非递归实现
    private static void search3(int[] a, int act) {
        int left = 0;
        int right = a.length-1;
        while (left <= right) {
            int mid = (left + right) / 2;
            if (a[mid] == act) {
                //找到该值
                System.out.println("找到该值");
                return;
            }
            if (act < a[mid]) {
                right = mid - 1;
                continue;
            }
            left = mid + 1;
        }
        System.out.println("没有找到该值");
    }
}
