package search;

import java.util.ArrayList;
import java.util.List;

/**
 * @author dali
 * @date 2021-03-10 15:16
 * @Description 插值查找
 */

public class InsertSearch {
    static List<Integer> list = new ArrayList();
    static int count = 0;
    public static void main(String[] args) {
        int[] a = {1,2,3,3,3,3,3,8,9,10};
        search(a,0,a.length-1,1);
        System.out.println("找到的数据有"+list.size()+"个");
        System.out.println("一共查找:"+count);
    }

    private static void search(int[] arr,int left, int right, int act) {
        count ++ ;
        if (left <= right) {
//            int mid = left + (right - left) * (act - arr[left]) / (arr[right] - arr[left]);
            int mid = (left + right) / 2;
            if (arr[mid] > act) {
                search(arr, left, mid, act);
            } else if (arr[mid] < act) {
                search(arr,mid+1, right, act);
            } else {
                System.out.println("找到数据");
                list.add(arr[mid]);
                int i = mid+1;
                if (i < arr.length) {
                    while (arr[i++] == arr[mid]) {
                        System.out.println("找到数据");
                        list.add(arr[mid]);
                    }
                }
                int j = mid-1;
                if (j > 0) {
                    while (arr[j--] == arr[mid]) {
                        System.out.println("找到数据");
                        list.add(arr[mid]);
                    }
                }
            }
        } else {
            System.out.println("没有该数据");
        }
    }
}
