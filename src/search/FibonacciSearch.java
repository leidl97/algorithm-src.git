package search;


import java.util.Arrays;

/**
 * @author dali
 * @date 2021-03-10 18:26
 * @Description 斐波那契算法
 */

public class FibonacciSearch {
    public static void main(String[] args) {
        int[] a = {1,8,10,89,1000,1234};
        sort(a,12314);
    }
    //{1,1,2,3,5,8,13,21}
    //因为后面需要使用斐波那契数列，分为递归方式与非递归的方式
    private static int[] fib() {
        int[] fib = new int[20];
        fib[0] = 1;
        fib[1] = 1;
        for (int i = 2; i < fib.length; i++) {
            fib[i] = fib[i-1] + fib[i-2];
        }
        return fib;
    }

    private static int sort(int[] arr, int act) {
        int low = 0;
        int high = arr.length - 1;
        int k = 0;//fib下标k
        int mid = 0;//黄金分割点
        int[] f = fib();//拿到斐波那契数列
        //获取到黄金分割点，数列长度大于数组长度且最接近的数组值
        while (high > f[k] - 1) {
            k++;
        }
        //f[k]的值可能大于数组长度，所以需要扩容。原数组的长度要接近斐波那契的值
        int[] temp = Arrays.copyOf(arr,f[k]-1);
        //将temp为0的位置填充
        for (int i = arr.length; i < temp.length; i++) {
            temp[i] = arr[high];
        }
        //循环查找
        while (low <= high) {
            mid = low + f[k-1] - 1;
            if (act < temp[mid]) {
                //向左查找
                high = mid - 1;
                //为什么是k--，全部 = 前面 + 后面，前面有F[K-1]个元素，可以继续拆分为f[k-2] + f[k-3]，在f[k-1]的前面继续查找
                k--;
            } else if(act > temp[mid]){
                //向右查找
                low = mid + 1;
                //为什么是k-2，全部 = 前面 + 后面，后面有f[k-2]个元素，将其拆分，需要在f[k-3]的位置进行查找，原来是k-1,现在是k-3所以要-2
                k -= 2;
            } else {
                //找到,需要进一步判断下标，防止越界
                if (mid < high) {
                    System.out.println("找到元素,下标为："+mid);
                    return 0;
                } else {
                    System.out.println("找到元素,下标为："+high);
                    return 0;
                }
            }
        }
        System.out.println("未找到元素");
        return -1;
    }
}
