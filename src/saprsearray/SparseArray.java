package saprsearray;

import javax.sound.midi.Soundbank;
import java.util.Arrays;

/**
 * @author dali
 * @date 2020-12-13 11:27
 * @Description 稀疏数组
 */

public class SparseArray {
    public static void main(String[] args) {
        //创建一个棋盘，0表示未落子，1表示黑子，2表示白子
        int chessArr[][] = new int[11][10];
        chessArr[1][2] = 1;
        chessArr[2][3] = 2;
        chessArr[3][4] = 1;
        int k = 0;
        //输出原始的二维数组
        for (int[] row : chessArr){
            for (int data : row){
                //按十进制输出+tab
                System.out.printf("%d\t",data);
                if (data != 0){
                    k++;
                }
            }
            System.out.println();
        }

        //转稀疏 row为列 col为行
        int row = chessArr[0].length;
        int col = chessArr.length;
        System.out.println("row="+row+"col="+col);
        int a = 1,b = 0;
        //创建稀疏数组
        int sparseArr[][] = new int[k+1][3];
        sparseArr[0][0] = col;
        sparseArr[0][1] = row;
        sparseArr[0][2] = k;
        for (int i = 0; i<chessArr.length; i++){
            for (int j = 0; j<chessArr[i].length; j++){
                if (chessArr[i][j] != 0){
                    sparseArr[a][0] = i;
                    sparseArr[a][1] = j;
                    sparseArr[a][2] = chessArr[i][j];
                    a++;
                }
            }
        }

        for (int[] row1 : sparseArr){
            for (int data : row1){
                //按十进制输出+tab
                System.out.printf("%d\t",data);
            }
            System.out.println();
        }
        //怎么遍历数组的行数与列数

    }
}
