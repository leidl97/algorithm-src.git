package queue;

/**
 * @author dali
 * @date 2020-12-16 19:07
 * @Description
 */

public class ArrayQueueDemo {
    public static void main(String[] args) {
        ArrayQueue aq = new ArrayQueue(3);
//        aq.getAllQueue();
        aq.addQueue(1);
        aq.addQueue(2);
        System.out.println(aq.getQueue());
//        System.out.println(aq.getQueue());
        System.out.println(aq.getBegin());
        System.out.println(aq.getQueue());
    }
}

class ArrayQueue{
    //数组最大容量
    private int maxSize;
    //队列头
    private int begin;
    //队列尾
    private int end;
    //存放队列的数组
    private int[] arr;

    public ArrayQueue(int maxSize){
        //this可以省略，但是写上显得思路更清晰
        this.maxSize = maxSize;
        this.arr = new int[maxSize];
        //队列头指向开始的前一个位置
        this.begin = -1;
        //队列尾指向最后的位置。虽然都是-1但表示的意义不同
        this.end = -1;
    }

    //判断队列是否未满,队列尾为数组最后一个元素
    public boolean isFull(){
        return this.end == this.maxSize - 1;
    }

    //判断队列是否为空 头尾指针相等的时候
    public boolean isEmpty(){
        return this.begin == this.end;
    }

    //添加数据到队列
    public void addQueue(int data){
        //判断是否满
        if (isFull()) {
            throw new RuntimeException("队列满，不能加入数据");
        }
        this.arr[++this.end] = data;
    }

    //获取队列的数据,取数据从后面取
    public int getQueue(){
        if (isEmpty()) {
            //抛出异常，不用在写return
            throw new RuntimeException("队列空，不可以取数据");
        }
        return this.arr[++this.begin];
    }

    //显示队列的所有数据
    public StringBuffer getAllQueue(){
        StringBuffer sb = new StringBuffer();
        if (isEmpty()) {
            throw new RuntimeException("该队列为空");
        }
        for (int i = 0; i< this.maxSize; i++) {
            sb.append(getQueue() + "\t");
        }
        return sb;
    }

    //显示队列的头数据
    public int getBegin(){
        if (isEmpty()) {
            throw new RuntimeException("该队列为空");
        }
        return this.arr[begin+1];
    }
}
