package queue;

/**
 * @author dali
 * @date 2020-12-21 19:35
 * @Description 环形队列（取模运算）
 */

public class CircleArrayQueueDemo {
    public static void main(String[] args) {
        //实际大小只有3
        CircleArrayQueue demo = new CircleArrayQueue(4);
        demo.addQueue(10);
        demo.addQueue(20);
        demo.addQueue(30);
//        linkedlist.addQueue(40);
        demo.getAllQueue();
        demo.getQueue();
        demo.getAllQueue();
        demo.addQueue(40);
        demo.getQueue();
        demo.getAllQueue();
        demo.addQueue(50);
        demo.getAllQueue();
//        linkedlist.addQueue(20);
//        linkedlist.addQueue(30);
//        linkedlist.getAllQueue();
    }
}

class CircleArrayQueue{
    //数组最大容量
    private int maxSize;
    //队列头
    private int begin;
    //队列尾
    private int end;
    //存放队列的数组
    private int[] arr;

    //如果按照头指向第一个元素，尾指向最后一个元素，当为起始状态时无法判断为空还是为满
    //定义添加数据尾指针移动，取数据头指针移动,这样设计牺牲了一个空间，也就是说默认最后一个位置不存数据
    public CircleArrayQueue(int maxSize){
        //this可以省略，但是写上显得思路更清晰
        this.maxSize = maxSize;
        this.arr = new int[maxSize];
        //队列头指向元素的第一个位置
        this.begin = 0;
        //队列尾指向元素最后的位置
        this.end = 0;
    }

    //判断队列是否为满,头+1%maxsize = 尾 ：头始终指向第一个元素当加1取模等于尾的时候说明满（尾表示最后一个元素+1）
    public boolean isFull(){
        return (this.end + 1)%maxSize == this.begin;
    }

    //判断队列是否为空 头尾指针相等的时候
    public boolean isEmpty(){
        return this.begin == this.end;
    }

    //添加数据到队列,尾指针+1
    public void addQueue(int data){
        //判断是否满
        if (isFull()) {
            throw new RuntimeException("队列满，不能加入数据");
        }
        arr[end] = data;
        end = (end + 1) % maxSize;
        System.out.println("存数据："+data);
    }

    //获取队列的数据,头指针+1
    public int getQueue(){
        if (isEmpty()) {
            //抛出异常，不用在写return
            throw new RuntimeException("队列空，不可以取数据");
        }
        int data = arr[begin];
        begin = (begin + 1) % maxSize;
        System.out.println("取数据:"+data);
        return data;
    }

    //显示队列的所有数据[注意取模]
    public void getAllQueue(){
        StringBuffer sb = new StringBuffer();
        if (isEmpty()) {
            throw new RuntimeException("该队列为空");
        }
        for (int i = begin; i< begin+getActive(); i++) {
            System.out.println("arr["+i%maxSize+"]="+arr[i%maxSize]);
        }
    }

    //显示队列的有效数据
    public int getActive(){
        return (end + maxSize - begin) % maxSize;
    }
}
