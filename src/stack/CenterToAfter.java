package stack;

import java.util.*;

/**
 * @author dali
 * @date 2021-01-21 19:12
 * @Description
 */

public class CenterToAfter {
    public static void main(String[] args) {
        //有多位数，加减乘除，小括号
        String before = "12+((2+3)*4)/4-5";//12, 2, 3, +, 4, *, 4, /, 5, -, +  / 12
        Queue<String> queue = todo(before);
        System.out.println("后缀转中缀的结果为：" + queue);
        String result = afterCal(queue);
        System.out.println("字符串表达式：【" + before + "】运算的结果为：" + result);
    }

    //中缀转后缀的算法
    public static Queue<String> todo(String before) {
        //创建栈s1
        Stack<String> stack = new Stack();
        //创建队列s2
        Queue<String> queue = new LinkedList();
        //第一步将传入的字符串转换为ArrayList
        //因为遍历起来好处理，处理多位数的情况
        List<String> list = stringToList(before);
        //遍历，按照规则进行
        for (int i = 0; i< list.size(); i++) {
            String cur = list.get(i);
            if (cur.matches("\\d+")){
                //如果为数字，则直接入队列
                queue.offer(cur);
            } else if ("(".equals(cur)){
                //如果为左括号，直接入栈
                stack.push(cur);
            } else if (stack.isEmpty()) {
                //如果栈为空则直接入栈
                stack.push(cur);
            } else if (")".equals(cur)) {
                //弹出栈中元素直到碰到左括号
                while (!stack.peek().equals("(")) {
                    queue.offer(stack.pop());
                }
                //非常重要！！！将左括号弹出栈，避免后续与运算符混合
                stack.pop();
            } else {
                //若栈顶元素不为左括号，则需要进行处理后入栈
                if (!stack.peek().equals("(")) {
                    int beforePriority = getPriority(stack.peek());
                    int nowPriority = getPriority(cur);
                    //若新运算符的优先级高，则直接入栈，否则将栈顶元素弹出到队列后入栈
                    if (nowPriority <= beforePriority) {
                        queue.offer(stack.pop());
                    }
                }
                stack.push(cur);
            }
        }

        //将栈中的剩余元素压入队列中
        while (!stack.isEmpty()) {
            queue.offer(stack.pop());
        }

        return queue;
    }

    //返回优先级，默认数字越大，优先级越高
    public static int getPriority(String s) {
        if ("+".equals(s) || "-".equals(s)) {
            return 0;
        } else if ("*".equals(s) || "/".equals(s)) {
            return 1;
        }else {
            throw new RuntimeException("暂不支持该运算符："+s);
        }
    }

    //字符串转集合的方法
    public static List<String> stringToList(String before){
        List<String> result = new ArrayList();
        char[] cs = before.toCharArray();
        String string = "";
        //判断上个是否为数字
        boolean flag = false;
        for (int i = 0; i < cs.length; i++) {
            if (cs[i] >= '0' && cs[i] <= '9') {
                //如果为数字，那么字符串相加
                string += cs[i];
                flag = true;
                if (i == cs.length-1) {
                    result.add(string);
                    break;
                }
            } else {
                if (flag) {
                    //先将数字部分存入集合
                    result.add(string);
                    string = "";
                }
                //那么不为数字，直接存入list
                result.add(cs[i] + "");
                flag = false;
            }
        }
        return result;
    }

    //后缀计算方法
    public static String afterCal(Queue<String> param) {
        //用来计算的栈
        Stack<String> stack = new Stack();
        //结果
        int result = 0;
        for (String str : param) {
            if (str.matches("\\d+")) {
                //如果为数字则直接入栈
                stack.push(str);
            } else {
                //为运算符，进行计算后入栈
                int num2 = Integer.parseInt(stack.pop());
                int num1 = Integer.parseInt(stack.pop());

                switch (str) {
                    case "+" :
                        result = num1 + num2;
                    break;
                    case "-" :
                        result = num1 - num2;
                    break;
                    case "*" :
                        result = num1 * num2;
                    break;
                    case "/" :
                        result = num1 / num2;
                    break;
                    default:
                        throw new RuntimeException("没有该运算符");
                }
                stack.push(String.valueOf(result));
            }
        }
        return stack.peek();
    }
}
