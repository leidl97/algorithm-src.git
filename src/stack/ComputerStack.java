package stack;

/**
 * @author dali
 * @date 2021-01-12 15:52
 * @Description 实际场景模拟，给一个字符串实现计算功能
 */

public class ComputerStack {
    public static void main(String[] args) {
        new ComputerStack().computer("12/3-10-1");
    }

    public boolean isPriority(char c,Object o) {
        //c的优先级
        int cPriority = Normal.getPriority(c);
        //栈中元素的优先级
        int oPriority = Normal.getPriority((char)o);
        System.out.println(cPriority+":"+oPriority);
        return (cPriority-oPriority) > 0;
    }

    //栈内最后运算,，最后只会剩下加减运算
    public void after(MyStack numStack, MyStack opreStack) {
        int num1 = (int)numStack.pop();
        int num2 = (int)numStack.pop();
        char c1 = (char)opreStack.pop();
        int result = 0;
        if (opreStack.top != -1) {
            //表示里面还有值，那么就去判断是否为-号，若为减法，则需要进行变号运算
            //对现有运算符进行变号
            if (c1 == '+') {
                c1 = (char)opreStack.stack[opreStack.top] == '-' ? '-' : c1;
            } else {
                c1 = (char)opreStack.stack[opreStack.top] == '-' ? '+' : c1;
            }
        }
        result = c1 == '+' ? num1 + num2 : num2 - num1;
        numStack.push(result);
    }

    //出栈计算操作
    public void before(char c, MyStack numStack, MyStack opreStack) {
        System.out.println("出栈计算");
        int num1 = (int)numStack.pop();
        int num2 = (int)numStack.pop();
        char c1 = (char)opreStack.pop();
        System.out.println(c);
        int result = 0;
        if (c1 == '*') {
            //后出来的操作之前出来的
            result = num1 * num2;
        } else if (c1 == '+') {
            result = num1 + num2;
        } else if (c1 == '-') {
            result = num2 - num1;
        }  else if (c1 == '/') {
            result = num2 / num1;
        } else {
            throw new RuntimeException("暂不支持该运算符");
        }
        //数字压入栈
        numStack.push(result);
        //操作符压入栈
        opreStack.push(c);
    }

    public void computer(String str) {
        char[] array = str.toCharArray();
        MyStack numStack = new MyStack(array.length);
        MyStack opreStack = new MyStack(array.length);
        int isNum = 0;
        //加入一个判断多位数的标志
        int count = 0;
        //创建一个存放多位数的数组,，默认最多十位数
        int[] duo = new int[10];
        for (int j = 0; j < array.length; j++){
            char c = array[j];
            if (c >= '0' && c <= '9') {
                isNum = 1;//表示为数字
            } else {
                //若不为数字，将对位数组进行处理，然后压入栈
                if (count != 0) {
                    int sum = 0;
                    for (int i = 0; i < count; i++){
                        sum = sum + duo[count-i-1]*((int)Math.pow(10,i));
                    }
                    numStack.push(sum);
                    count = 0;
                }
            }
            switch (isNum) {
                case 0 :
                    if (opreStack.isEmpty()){
                        opreStack.push(c);
                        break;
                    }
                    if (isPriority(c,opreStack.stack[opreStack.top])) {
                        //如果传入的比栈中的优先级高则直接压入栈
                        System.out.println("压入栈");
                        opreStack.push(c);
                        break;
                    }
                    //否则进行出栈计算操作
                    before(c,numStack,opreStack);
                    break;
                default:
                    duo[count] = c - '0';
                    count++;
                    //如果是遍历的最后一个元素则直接压入栈
                    if (j == array.length - 1) {
                        numStack.push(c - '0');
                    }
                    break;
            }
            isNum = 0;
        }
        while (!opreStack.isEmpty()) {
            after(numStack, opreStack);
        }
        System.out.println(numStack);
        System.out.println(opreStack);
        System.out.println("该字符串["+str+"]运算的结果为："+numStack.stack[numStack.top]);
    }
}

class Normal {
    //数字越大，优先级越高
    public static final int plusPriority = 1;
    public static final int subPriority = 1;
    public static final int multiply = 2;


    public static int getPriority(char c) {
        int priority = 0;
        switch (c) {
            case '*' :
                priority = Normal.multiply;
                break;
            case '/' :
                priority = Normal.multiply;
                break;
            case '+' :
                priority = Normal.plusPriority;
                break;
            case '-' :
                priority = Normal.subPriority;
                break;
            default:
                priority = 0;
        }
        return priority;
    }
}
