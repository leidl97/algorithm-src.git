package stack;

import java.util.Arrays;

/**
 * @author dali
 * @date 2021-01-11 19:48
 * @Description 用数组来模拟栈
 */

public class ArrayStack {
    public static void main(String[] args) {
        MyStack stack = new MyStack(5);
        stack.push(1);
        stack.push(2);
        stack.pop();
        stack.pop();
        stack.pop();
    }
}

class MyStack {
    //栈顶
    public int top;
    //栈
    public Object[] stack;
    //栈的大小
    public int size;

    public MyStack(int size) {
        this.top = -1;
        this.size = size;
        this.stack = new Object[this.size];
    }

    //入栈
    public void push(Object data) {
        if (isFull()) {
            System.out.println("栈满");
            return;
        }
        this.top++;
        this.stack[top] = data;
    }

    //出栈
    public Object pop() {
        if (isEmpty()) {
            System.out.println("栈空");
            return null;
        }
        Object val = this.stack[this.top];
        this.stack[top] = 0;
        this.top--;
        System.out.println("出栈成功："+val);
        return val;
    }

    //栈满
    public boolean isFull() {
        return this.top == this.size - 1;
    }

    //栈空
    public boolean isEmpty() {
        return this.top == -1;
    }

    @Override
    public String toString() {
        return "Stack{" +
                "top=" + top +
                ", stack=" + Arrays.toString(stack) +
                '}';
    }
}
