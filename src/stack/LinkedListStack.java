package stack;

import javax.crypto.interfaces.PBEKey;
import java.time.temporal.Temporal;

/**
 * @author dali
 * @date 2021-01-11 20:18
 * @Description 用链表来模拟栈
 */

public class LinkedListStack {
    public static void main(String[] args) {
        MangerStack mangerStack = new MangerStack();
        MyLinkedListStack stack1 = new MyLinkedListStack(1);
        MyLinkedListStack stack2 = new MyLinkedListStack(2);
        MyLinkedListStack stack3 = new MyLinkedListStack(3);
        mangerStack.pushHead(stack1);
        mangerStack.pushHead(stack2);
        mangerStack.pushHead(stack3);
        mangerStack.popHead();
        mangerStack.popHead();
        mangerStack.popHead();
        mangerStack.popHead();
    }
}

class MangerStack {
    public MyLinkedListStack head = new MyLinkedListStack(0);

    //入栈---尾插法
    public void push(MyLinkedListStack stack) {
        MyLinkedListStack cur = head;
        while (true) {
            if (cur.next == null) {
                break;
            }
            cur = cur.next;
        }
        cur.next = stack;
    }

    //出栈---尾插法
    //分情况讨论，如果没有元素则提示栈空，如果只有一个元素则让该元素出栈，将该节点置为空
    public void pop() {
        if (head.next == null) {
            System.out.println("栈空");
            return;
        }
        if (head.next.next == null) {
            System.out.println("出栈元素为："+head.next);
            head.next = null;
            return;
        }
        MyLinkedListStack cur = head;
        while (true) {
            if (cur.next.next == null) {
                System.out.println("出栈元素为："+cur.next);
                cur.next = null;
                break;
            }
            cur = cur.next;
        }
    }

    //出栈---头插法
    public void popHead() {
        if (head.next == null) {
            System.out.println("栈空");
            return;
        }
        System.out.println("出栈元素为："+head.next);
        head.next = head.next.next;
    }

    //入栈---头插法
    public void pushHead(MyLinkedListStack stack) {
        stack.next = head.next;
        head.next = stack;
    }

    //遍历
    public void show(){
        MyLinkedListStack cur = head;
        while (true) {
            if (cur.next == null) {
                break;
            }
            cur = cur.next;
            System.out.println(cur);
        }
    }
}

class MyLinkedListStack {
    public int no;
    public MyLinkedListStack next;
    public char data;

    public MyLinkedListStack(int no) {
        this.no = no;
    }

    public MyLinkedListStack(char data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "MyLinkedListStack{" +
                "no=" + no +
                '}';
    }
}
