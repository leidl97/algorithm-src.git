package linkedlist;

import com.sun.org.apache.regexp.internal.RE;

/**
 * @author dali
 * @date 2020-12-29 18:44
 * @Description
 */

public class SingleLinkedList {
    public static void main(String[] args) {
        MangerNode mangerNode = new MangerNode();
        Node node1 = new Node(1, "1号老婆");
        Node node2 = new Node(2, "2号老婆");
        Node node3 = new Node(3, "3号老婆");
        Node node4 = new Node(4, "4号老婆");
//        mangerNode.add(node1);
//        mangerNode.add(node3);
//        mangerNode.add(node2);
//        mangerNode.add(node4);
        mangerNode.addPro(node1);
//        mangerNode.addPro(node3);
//        mangerNode.addPro(node2);
        mangerNode.delete(node4);
        mangerNode.show();
//        System.out.println("反转");
//        mangerNode.headReverse();
//        mangerNode.reverse(mangerNode.head);
//        mangerNode.show();
        System.out.println("自己的反转");
        System.out.println(mangerNode.myReverse(mangerNode.head));
    }
}
//管理节点类
class MangerNode{
    //头节点，定义后不变
    public Node head = new Node(0,"");
    //辅助节点
    public Node temp;

    //添加一个节点
    public void add(Node node){
        temp = head;
        while (true) {
            if (temp.next == null) {
                break;
            }
            temp = temp.next;
        }
        temp.next = node;
    }

    //添加一个节点(高级版)，按照顺序添加
    public void addPro(Node node){
        temp = head;
        //默认没有重复元素
        boolean flag = false;
        while (true) {
            if (temp.next == null) {
                break;
            }
            if (temp.next.no > node.no) {
                break;
            } else if (temp.next.no == node.no) {
                flag = true;
                break;
            }
            temp = temp.next;
        }
        //添加元素
        if (flag) {
            System.out.println("有no为"+node.no+"重复元素不可以添加");
        } else {
            node.next = temp.next;
            temp.next = node;
        }
    }

    //找到修改即可
    public void update(Node node){
        if (head.next == null) {
            System.out.println("链表为空！");
        } else {
            temp = head.next;
            while (true) {
                if (temp.next == null){
                    break;
                }
                if (temp.no == node.no) {
                    temp.data = node.data;
                    break;
                }
                temp = temp.next;
            }
        }
    }

    //找到A指向B成为指向C然后删除B
    public void delete(Node node){
        if (head.next == null) {
            System.out.println("链表为空！");
        } else {
            temp = head;
            while (true) {
                if (temp.next == null){
                    break;
                }
                if (temp.next.no == node.no) {
                    temp.next = temp.next.next;
                    break;
                }
                temp = temp.next;
            }
        }
    }

    //单链表的遍历
    public int foreach(){
        int count = 0;
        temp = head.next;
        if (temp == null) {
            System.out.println("链表为空");
        } else {
            //表示元素至少一个
            count++;
            while (true){
                if (temp.next == null) {
//                    System.out.println("遍历结束");
                    break;
                }
                count++;
                temp = temp.next;
            }
        }
//        System.out.println("一共有"+count+"个元素");
        return count;
    }

    //单链表的反转,将原链表的数据从后到前取出然后一次添加到新链表中
    public MangerNode reverse(){
        int count = foreach();
        //临时存储变量
        Node swap;
        //创建一个辅助节点
        Node temp;
        //创建一个新的管理节点
        MangerNode mangerNode = new MangerNode();
        if (count != 0) {
            temp = head.next;
            for (int i = 0; i < count; i++) {
                for (int j = 0; j < count - i - 1; j++) {
                    temp = temp.next;
                }
                //赋值
                swap = new Node(temp.no, temp.data);
                mangerNode.add(swap);
//                node.next = swap;
//                node = node.next;
                //初始化
                temp = head.next;
            }
        }
        mangerNode.show();
        return mangerNode;
    }

    //单链表的反转，头插法，将原链表中的数据取出后一次插入最前面的位置，不只是数据改变，地址也不可以改变
    //自己写的先作废
    public void headReverse(){
        int count = foreach();
        //新的头节点
        Node headCurrent = new Node(0,"");
        //创建辅助节点，作为新节点的辅助变量
        Node tempHead = headCurrent;
        //创建一个辅助节点,存储原链表中的节点
        Node temp = head.next;
        //创建一个新的管理节点
        MangerNode mangerNode = new MangerNode();
        if (count != 0) {
            while (true) {
                if (temp.next == null) {
                    break;
                }
                //赋值,取出的下一个节点指向，原节点的下一个节点
                temp.next = headCurrent.next;
                //原节点的下一个节点指向新节点
                headCurrent.next = temp;
                mangerNode.add(tempHead);
                //下一个
                temp = head.next;
            }
        }
        mangerNode.show();
    }

    //老师的方法
    public void reverse(Node headNode){
        //当没有节点或者只有一个节点的时候直接返回结果
        if (headNode.next == null || headNode.next.next == null) {
            System.out.println("链表为空！");
        }

        //定义辅助变量cur，用于遍历原链表
        Node cur = headNode.next;
        //定义cur的下一个节点next，用来记录
        Node next = null;
        //定义新节点的头节点
        Node newHead = new Node(0,"");
        while (cur != null) {
            //next指向cur的下一个节点
            next = cur.next;
            //cur的下一个节点指向新头节点的下一个节点
            cur.next = newHead.next;
            //新头节点的下一个节点指向cur节点
            newHead.next = cur;
            //cur后移
            cur = next;
        }
        //此时cur已经是head之后反转的一串链表，从新节点给到原节点上，实现原节点的反转
        headNode.next = newHead.next;
    }

    public Node myReverse(Node head){
        if (head.next == null || head.next.next == null) {
            throw new RuntimeException("链表为空或者只有一个节点不需要反转！");
        }
        //定义遍历辅助变量
        Node temp = head.next;
        //定义遍历的下一个节点用于储存位置
        Node next = null;
        //定义新头节点
        Node newHead = new Node(0,"");
        //开始遍历
        while (temp != null) {
            //next赋值
            next = temp.next;
            //遍历当前节点的下一个指向新头节点的后面，next转移
            temp.next = newHead.next;
            //新节点的下一个指向当前遍历节点
            newHead.next = temp;
            //temp后移
            temp = next;
        }
        head.next = newHead.next;
        return head;
    }


    //遍历链表
    public void show(){
        temp = head.next;
        //判断是否为空
        if(temp == null) {
            System.out.println("链表为空");
        } else {
            while (true) {
                System.out.println(temp);
                if (temp.next == null){
                    break;
                }
                temp = temp.next;
            }
        }
    }

}

//定义节点
class Node{
    //标记当前节点序号
    public int no;
    //数据域
    public String data;
    //下一个节点
    public Node next;

    public Node(int no, String data){
        this.no = no;
        this.data = data;
    }

    @Override
    public String toString() {
        return "Node{" +
                "no=" + no +
                ", data='" + data + '\'' +
                ", next=" + next +
                '}';
    }
}
