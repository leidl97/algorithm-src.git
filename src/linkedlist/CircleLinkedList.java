package linkedlist;

import java.time.temporal.Temporal;

/**
 * @author dali
 * @date 2021-01-11 14:38
 * @Description 环形单向链表，约瑟夫问题
 */

public class CircleLinkedList {
    public static void main(String[] args) {
        MangerCircleNode mangerCircleNode = new MangerCircleNode(0,"大老婆");
        CircleNode circleNode1 = new CircleNode(1,"1号老婆");
        CircleNode circleNode2 = new CircleNode(2,"2号老婆");
        CircleNode circleNode3 = new CircleNode(3,"3号老婆");
        CircleNode circleNode4 = new CircleNode(4,"4号老婆");
//        mangerCircleNode.add(circleNode1);
//        mangerCircleNode.add(circleNode2);
//        mangerCircleNode.add(circleNode3);
//        mangerCircleNode.add(circleNode4);
//        mangerCircleNode.yusefu(3);
        new MangerCircleNode().yusefu(2,2,5);
    }
}

class MangerCircleNode {
    //设置第一个节点
    public CircleNode first;
    //辅助节点
    public CircleNode cur;

    public MangerCircleNode(int no, String data) {
        first = new CircleNode(no, data);
        first.next = first;
    }

    public MangerCircleNode(int size) {
        MangerCircleNode m = addByNum(size);
        this.first = m.first;
    }

    public MangerCircleNode() {

    }

    //指定一个大小的环形链表
    public MangerCircleNode addByNum(int num) {
        if (num < 1) {
            System.out.println("大小不能小于1！");
            return null;
        }
        MangerCircleNode mangerCircleNode = new MangerCircleNode();
        CircleNode newNode = null;
        for (int i = 1; i<=num; i++) {
            if (i == 1){
                //初始化
                mangerCircleNode.first = new CircleNode(i,"老婆"+i+"号");
                mangerCircleNode.first.next = mangerCircleNode.first;
                mangerCircleNode.cur =mangerCircleNode.first;
            } else {
                //添加环形节点
                newNode = new CircleNode(i,"老婆"+i+"号");
                mangerCircleNode.cur.next = newNode;
                newNode.next = mangerCircleNode.first;
            }
            //cur指向下一个节点
            mangerCircleNode.cur = mangerCircleNode.cur.next;
        }
        return mangerCircleNode;
    }

    /**
     *
     * @param startNo 从第几个数
     * @param countNo 数几次
     * @param size 一共多少人
     */
    public void yusefu(int startNo, int countNo, int size){
        if (startNo < 1 || countNo < 1 || size < 1 || startNo > size) {
            System.out.println("输入的数字不合理！");
            return;
        }
        MangerCircleNode mangerCircleNode = new MangerCircleNode(size);
        //确认first为哪一个节点，此时已经是一个环，循环获取即可
        for (int i = 0; i<startNo-1; i++) {
            mangerCircleNode.first = mangerCircleNode.first.next;
        }
        //复位
        mangerCircleNode.cur = mangerCircleNode.first;
        while (true) {
            if (size == 0) {
                System.out.println("最后出圈的编号为："+mangerCircleNode.cur.no);
                break;
            }
            for (int j = 0; j<countNo-1; j++){
                mangerCircleNode.cur = mangerCircleNode.cur.next;
            }
            System.out.println(mangerCircleNode.cur);
            mangerCircleNode.first = mangerCircleNode.cur.next;
            mangerCircleNode.delete(mangerCircleNode.cur);
            mangerCircleNode.cur = mangerCircleNode.first;
            size--;
        }
    }

       //获得该链表的个数
    public int getCount() {
        cur = first;
        int count = 0;
        while (true) {
            count++;
            if(cur.next == first){
                break;
            }
            cur = cur.next;
        }
        return count;
    }

    //删除一个节点
    public void delete(CircleNode circleNode) {
        cur = first;
        while (true) {
            if (cur.next.no == circleNode.no) {
                if (cur.next == first) {
                    first = cur.next.next;
                    cur.next = first;
                    System.out.println("删除的数据为头节点，当前头节点顺延为"+first);
                    break;
                }
                cur.next = cur.next.next;
                System.out.println("数据删除成功！");
                break;
            }
            if (cur.next == first) {
                System.out.println("未找到需要删除的数据!");
                break;
            }
            cur = cur.next;
        }
    }

    //修改一个节点
    public void update(int no, String data) {
        cur = first;
        while (true) {
            if (cur.no == no) {
                cur.data = data;
                System.out.println("数据修改成功！");
                break;
            }
            if (cur.next == first) {
                System.out.println("未找到需要修改的数据!");
                break;
            }
            cur = cur.next;
        }
    }

    //添加一个节点
    public void add(CircleNode circleNode) {
        cur = first;
        while (true) {
            if (cur.next == first) {
                break;
            }
            cur = cur.next;
        }
        cur.next = circleNode;
        circleNode.next = first;
    }

    //遍历这个节点
    public CircleNode show() {
        //cur复原到first的位置
        cur = first;
        while (true) {
            System.out.println(cur);
            if (cur.next == first) {
                break;
            }
            cur = cur.next;
        }
        return cur;
    }
}

class CircleNode {
    //节点编号
    public int no;
    //数据域
    public String data;
    //下一个节点
    CircleNode next;

    public CircleNode(int no, String data) {
        this.no = no;
        this.data = data;
    }

    @Override
    public String toString() {
        return "CircleNode{" +
                "no=" + no +
                ", data='" + data + '\'' +
                '}';
    }
}
