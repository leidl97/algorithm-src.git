package linkedlist;

import javafx.scene.control.cell.PropertyValueFactory;

import java.awt.*;

/**
 * @author dali
 * @date 2021-01-05 18:55
 * @Description
 */

public class DoubleLinkedList {
    public static void main(String[] args) {
        DoubleNode node1 = new DoubleNode(1,"老婆1号");
        DoubleNode node2 = new DoubleNode(2,"老婆2号");
        DoubleNode node3 = new DoubleNode(3,"老婆3号");
        //更新节点
        DoubleNode node4 = new DoubleNode(1,"老婆4号");
        ManagerDoubleNode.add(node1);
        ManagerDoubleNode.add(node2);
        ManagerDoubleNode.add(node3);
//        ManagerDoubleNode.delete(node3.getNo());
        ManagerDoubleNode.update(node4);
        ManagerDoubleNode.show();
    }
}

class DoubleNode {
    //定义节点编号
    private int no;
    //定义前一个节点
    public DoubleNode pre;
    //定义数据
    public String data;
    //定义后一个节点
    public DoubleNode next;

    DoubleNode(int no, String data) {
        this.no = no;
        this.data = data;
    }

    public int getNo() {
        return no;
    }

    //toString输出如果有pre和next会报栈溢出
    @Override
    public String toString() {
        return "DoubleNode{" +
                "no=" + no +
                ", data='" + data + '\'' +
                '}';
    }
}

class ManagerDoubleNode {
    public static DoubleNode head = new DoubleNode(0,"");
    //增加一个节点
    public static void add(DoubleNode node) {
        DoubleNode temp = ManagerDoubleNode.head;
        while (temp.next != null) {
            temp = temp.next;
        }
        temp.next = node;
        node.pre = temp;
    }
    //删除一个节点
    public static void delete(int no) {
        DoubleNode temp = ManagerDoubleNode.head.next;
        boolean flag = false;
        while (temp != null) {
            if (temp.getNo() == no) {
                //找到该节点，进行删除
                flag = true;
                temp.pre.next = temp.next;
                if (temp.next != null) {
                    //如果该节点不是最后一个节点，则它的下一个节点指向temp的前一个节点
                    temp.next.pre = temp.pre;
                }
                break;
            }
            //如果找不到就挪到下一个位置
            temp = temp.next;
        }
        if (flag) {
            System.out.println("该节点已删除！");
        } else {
            System.out.println("该节点不存在，无法删除！");
        }
    }

    public static void update(DoubleNode node) {
        DoubleNode temp = ManagerDoubleNode.head.next;
        boolean flag = false;
        while (temp != null) {
            if (temp.getNo() == node.getNo()) {
                //找到该节点，进行更新操作
                flag = true;
                temp.data = node.data;
                break;
            }
            //如果找不到就挪到下一个位置
            temp = temp.next;
        }
        if (flag) {
            System.out.println("该节点已更新！");
        } else {
            System.out.println("该节点不存在，无法更新！");
        }
    }

    public static void show() {
        DoubleNode temp = ManagerDoubleNode.head.next;
        while (temp != null) {
            System.out.println(temp);
            temp = temp.next;
        }
    }
}
