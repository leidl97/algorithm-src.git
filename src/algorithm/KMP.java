package algorithm;

import java.util.Arrays;

/**
 * @author dali
 * @date 2021-04-20 13:29
 * @Description KMP字符串匹配算法，存在返回第一个字符下标，否则返回-1
 */

public class KMP {
    public static void main(String[] args) {

        //目标串
        String str1 = "BBC ABCDAB ABCDABCDABDE";
        //模式串
        String str2 = "ABCDABD";

        String temp = "AACDAAA";

        System.out.println(Arrays.toString(next(str2)));
    }

    public static int cal(String str1, String str2) {
        //创建目标串指针
        int i = 0;
        //创建模式串指针
        int j = 0;
        //获得匹配表
        int[] next = next(str2);
        while (i < str1.length()) {
            if (j == str2.length()) {
//                return i - str2.length();
                //换为下面的更有效率
                return i - j;
            }
            if (str1.charAt(i) == str2.charAt(j)) {
                i++;
                j++;
            } else if (j == 0){
                //如果没有匹配成功，那么直接将i向后移动
                i++;
            } else {
                //否则表示匹配过程中失败，将i退回到相应的位置
                i = i - next[j-1];
                j = 0;
            }
        }
        return -1;
    }

    //生成next数组
    public static int[] next(String str) {
        int[] result = new int[str.length()];
        result[0] = 0;
        for (int i = 1, j = 0; i < str.length(); i++) {
            while (j > 0 && str.charAt(i) != str.charAt(j)) {
                j = result[j - 1];
//                j = 0;
            }
            if (str.charAt(i) == str.charAt(j)) {
                j++;
            }
            result[i] = j;
        }
        return result;
    }

 }
