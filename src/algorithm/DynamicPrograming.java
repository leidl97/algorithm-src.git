package algorithm;


/**
 * @author dali
 * @date 2021-04-14 14:45
 * @Description 动态规划 0-1背包问题
 */

public class DynamicPrograming {
    public static void main(String[] args) {
        //价值
        int[] v = {1500,3000,2000};
        //重量
        int[] w = {1,4,3};
        //结果数组
        int[][] result = new int[v.length + 1][4 + 1];

        //第一列为0
        for (int i = 0; i< result.length; i++) {
            result[i][0] = 0;
        }

        //第一行为0
        for (int i = 0; i< result[0].length; i++) {
            result[0][i] = 0;
        }

        for (int i = 1; i < result.length; i++) {
            for (int j = 1; j < result[0].length; j++) {
                //判断重量
                if (w[i-1] > j) {
                    //大于给定重量，那么只能不选，采用上方的
                    result[i][j] = result[i-1][j];
                } else {
                    //在不选和选取较大值
                    result[i][j] = Math.max(result[i-1][j], v[i-1] + result[i-1][j - w[i-1]]);
                }
            }
        }

        new DynamicPrograming().print(result);

        int i = result.length-1;
        int j = result[0].length-1;
        while (i > 0 && j > 0) {
            if (result[i][j] != result[i-1][j]) {
                //不等于上面的，那么该物品装入背包
                System.out.println(i+"号物品装入背包");
                j -= w[--i];
            } else {
                i--;
            }
        }

    }

    //打印当前二维数组
    void print(int[][] arr) {
        for (int[] ns : arr) {
            for (int i : ns) {
                System.out.print(i + "\t");
            }
            System.out.println();
        }
    }
}
