package algorithm;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

/**
 * @author dali
 * @date 2021-07-01 21:50
 * @Description 骑士周游，马踏棋盘问题
 */

public class Horse {
    static int count = 0;
    //用到了回溯的思想，两种方法，一种DFS，一种贪心思想
    public static void main(String[] args) {
        int[][] act = new int[6][6];
        DFS(act,1,3,1);
//        int[][] act = new int[8][8];
//        DFS(act,4,4,1);
        print(act);
    }

    //回溯思想
    static void DFS(int[][] act,int i, int j,int step) {
        act[i][j] = step;
        count ++;
//        if (count == 11195506) {
//            count--;
//        }
        if (step == act.length * act.length) {
            //如果达到预定步数，则打印
            print(act);
        }
        //得到可以落子的坐标
        List<Luozi> ls = next(act, i, j);
        //贪心思想，每次都尽量选取返回数量最小的，以减少回溯的次数
        sort(ls,act);
//        print(act);
        for (Luozi l : ls) {
            DFS(act,l.i,l.j,step+1);
        }
        //如果当前没有达到预定步数，将此点置为0，进行回溯
        if (step < act.length * act.length) {
            act[i][j] = 0;
        }
    }

    //进行递增排序，贪心思想
    private static void sort(List<Luozi> ls,int[][] act) {
        ls.sort(new Comparator<Luozi>() {
            @Override
            public int compare(Luozi o1, Luozi o2) {
                int a = next(act, o1.i, o1.j).size();
                int b = next(act, o2.i, o2.j).size();
                if (a < b) {
                    return -1;
                } else if(a == b) {
                    return 0;
                } else {
                    return 1;
                }
            }
        });
    }

    //打印邻接矩阵
    private static void print(int[][] act) {
        for (int[] ns : act) {
            for (int i : ns) {
                System.out.print(i + "\t");
            }
            System.out.println();
        }
        System.out.println("-----------------------");
    }

    //i，j表示马的位置
    static List<Luozi> next(int[][] act, int i, int j) {
        int size = act.length;
        List<Luozi> ls = new ArrayList<>();
        //四象限
        if (i+1 < size && j+2 < size && act[i+1][j+2] ==0) {
            ls.add(new Luozi(i+1,j+2));
        }
        if (i+2 < size && j+1 < size && act[i+2][j+1] ==0) {
            ls.add(new Luozi(i+2,j+1));
        }
        //三象限
        if (i+1 < size && j-2 >= 0 && act[i+1][j-2] ==0) {
            ls.add(new Luozi(i+1,j-2));
        }
        if (i+2 < size && j-1 >= 0 && act[i+2][j-1] ==0) {
            ls.add(new Luozi(i+2,j-1));
        }
        //一象限
        if (i-1 >= 0 && j-2 >= 0 && act[i-1][j-2] ==0) {
            ls.add(new Luozi(i-1,j-2));
        }
        if (i-2 >= 0 && j-1 >= 0 && act[i-2][j-1] ==0) {
            ls.add(new Luozi(i-2,j-1));
        }
        //二象限
        if (i-1 >= 0 && j+2 < size && act[i-1][j+2] ==0) {
            ls.add(new Luozi(i-1,j+2));
        }
        if (i-2 >= 0 && j+1 < size && act[i-2][j+1] ==0) {
            ls.add(new Luozi(i-2,j+1));
        }
        return ls;
    }
}

class Luozi {
    int i;
    int j;

    Luozi(int i, int j) {
        this.i = i;
        this.j = j;
    }

    @Override
    public String toString() {
        return "Luozi{" +
                "i=" + i +
                ", j=" + j +
                '}';
    }
}
