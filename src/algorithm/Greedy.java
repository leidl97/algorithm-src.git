package algorithm;

import java.util.*;

/**
 * @author dali
 * @date 2021-04-21 10:36
 * @Description 贪心算法
 */

public class Greedy {
    public static void main(String[] args) {
        Map<String,String> radio = new HashMap<>();
        radio.put("k1","上海，天津");
        radio.put("k2","北京，广州，深圳");
        radio.put("k3","成都，上海，杭州");
        radio.put("k4","北京，上海，天津");
        radio.put("k5","杭州，大连");
        //存放方案结果集
        List<String> result = new ArrayList<>();
        //获得所有地区
        List<String> allArea = new ArrayList<>(getAllArea(radio));
        System.out.println(allArea);
        List<String> temp = new ArrayList<>();
        while (allArea.size()>0) {
            String max = null;
            for (Map.Entry<String,String> entry : radio.entrySet()) {
                String k = entry.getKey();
                String v = entry.getValue();
                String[] ss = v.split("，");
                for (String str : ss) {
                    temp.add(str);
                }
                temp.retainAll(allArea);
                if (temp.size() > 0 && (max == null || temp.size() > radio.get(max).split("，").length)) {
                    max = k;
                }
                temp.clear();
            }
            //将方案添加到结果集中
            result.add(max);
            for (String str : radio.get(max).split("，")) {
                allArea.remove(str);
            }
//            radio.remove(max);

        }
        System.out.println(result);
    }

    //获得所有地区
    static Set<String> getAllArea(Map<String,String> radio) {
        Set<String> allArea = new HashSet<>();
        radio.forEach((k,v) -> {
            String[] s = v.split("，");
            for (String str : s) {
                allArea.add(str);
            }
        });
        return allArea;
    }
}
