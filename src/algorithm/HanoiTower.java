package algorithm;

/**
 * @author dali
 * @date 2021-04-09 13:59
 * @Description 汉诺塔
 */

public class HanoiTower {
    public static void main(String[] args) {
        merge(3,'A','B','C');
    }
    /**
     * 汉诺塔移动
     * @param n 几个盘子
     * @param a 源柱
     * @param b 辅助柱
     * @param c 目标柱
     */
    public static void merge(int n, char a, char b, char c) {
        if (n == 1) {
            System.out.println("第1个盘从"+a+"移到"+c);
            return;
        }
        if (n >= 2) {
            merge(n-1,a,c,b);
            System.out.println("第"+n+"个盘从"+a+"移到"+c);
            merge(n-1,b,a,c);
        }
    }
}
